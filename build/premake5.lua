-- premake5.lua
workspace "RL"
   configurations { "Debug", "Release" }

project "RL"
   kind "ConsoleApp"
   
   language "C++"
   
   -- Default from the example
   targetdir "bin/%{cfg.buildcfg}"
   
   -- Get headers from all directories
   includedirs { "../include/" }
   includedirs { "../src/" }

   -- I screwed up by making this in Visual Studio, doesn't use main :^)
   entrypoint ("RL")
   
   -- Recurse /src, and include because pathing isn't just headers :(
   files { "../src/**.h", "../src/**.cpp", "../include/**.h", "../include/**.cpp"}
   
   -- Eventually make this static, but not now
   links { "pthread", "sfml-system", "sfml-window", "sfml-graphics", "thor"}

   --
   flags { "C++14" }

   newoption {
      trigger     = "Debug",
      description = "Get debug symbols"
   }

   newoption {
      trigger     = "Release",
      description = "Make it faster"
   }

   filter "Debug"
      defines { "DEBUG" }
      symbols "On"

   filter "Release"
      defines { "NDEBUG" }
      optimize "Speed" -- clang -O2

   --filter "configurations:Debug"
   --    defines { "DEBUG" }
   --    symbols "On"

   -- filter "configurations:Release"
   --    defines { "NDEBUG" }
   --    optimize "Speed"