#include "LoadState.h"

LoadState::LoadState(float & progress) : progress(progress)
{
	//TODO: make use UIText, make it modular
	typedef sf::Vector2f v2f;

	sf::Vector2u v = Engine::Window->getSize();
	v2f chunk = (v2f)v/(chunksize);
	
	background = sf::RectangleShape((v2f)v);
	background.setPosition(0, 0);
	background.setFillColor(sf::Color(0, 0, 0, 100));

	loadProgress.setSize(v2f(0, barHeight * chunk.y));
	loadProgress.setPosition(sf::Vector2f(v.x / 2 - chunk.x*barWidth / 2, v.y / 2 - chunk.y*barHeight / 2));
	loadProgress.setFillColor(sf::Color::Black);

	loadBorder.setSize(sf::Vector2f(chunk.x*barWidth, chunk.y*barHeight) + loadBorderSize*2.0f);
	loadBorder.setPosition(loadProgress.getPosition() - loadBorderSize);
	loadBorder.setFillColor(sf::Color::White);

	loadingText.setFont((*Engine::Fonts)["main"]);
	loadingText.setString("Loading...");
	loadingText.setFillColor(sf::Color::White);
	//center text
	sf::FloatRect textRect = loadingText.getLocalBounds();
	loadingText.setOrigin(textRect.left + textRect.width / 2.0f,
	textRect.top + textRect.height / 2.0f);
	loadingText.setPosition(v2f(v.x/2.0f, v.y/2.0f-100));

	//loadingText.setStyle(sf::Text::Underlined);
}

void LoadState::enter()
{
}

void LoadState::loadContent()
{
	//no content...?
}

void LoadState::update()
{
	typedef sf::Vector2f v2f;
	sf::Vector2u v = Engine::Window->getSize();
	v2f chunk = (v2f)v/(chunksize);
	loadProgress.setSize(v2f(progress*chunk.x*barWidth, barHeight*chunk.y));
	//Engine::Console()->warn("Reported progress {}", progress);
	if (progress > 1.0) {
		exit();
	}
}

void LoadState::draw()
{
	Engine::Window->draw(background);
	Engine::Window->draw(loadBorder);
	Engine::Window->draw(loadProgress);
	Engine::Window->draw(loadingText);
}

void LoadState::exit()
{
}
