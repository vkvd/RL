#include "States/GameState.h"
#include "Item.h"
#include "Systems/SystemsDefines.h" // very bad idea
#include "Engine/Components.h" // EXTREMELY bad idea
#include "Engine/Engine.h" // what are you doing
#include "Thor/Time.hpp"

// TODO: REFACTOR: why are weapons responsible for so much?
// really bad OOP here, if it works dont fix it i guess........

// TODO: load hitboxes from JSON or something hardcoding them is honestly just awful...
// at least break it into a seperate file...

Item::Item() {

}

Item::~Item(){

}

Weapon::Weapon() : Weapon(WeaponType::Sword, Random(0,1,3,0)){

}

Weapon::~Weapon() {}
 
Weapon::Weapon(WeaponType type, Random damage) : damage(damage){
// TODO: lua loading/JSON loading, either would be cool
	#define v2 sf::Vector2f
	switch(type){
	case WeaponType::Sword: 
		Engine::Console()->info("Creating sword");
		data.cooldowns      = std::vector<float>({0.3f, 0.3f, 0.8f});
		data.swingCount     = 3;
		data.cSwingCount    = 0;
		data.isSwinging     = false;
		data.windups        = std::vector<float>({0.00f, 0.00f, 0.4f});
		data.swingInterrupt = 0.35f;
		// Hitboxes: Assumes 0,0 is the player. Negative is up (at cursor)/left, positive is down (away from cursor)/right;
		data.hitboxes["swing"] = 
			HitboxSeries({
				Hitbox({
					Triangle({
						v2(0.0f,0.0f),
						v2(50.0f,-100.0f),
						v2(-50.0f,-100.0f)
					}),
					Triangle({
						v2(50.0f,-100.0f),
						v2(-50.0f,-100.0f),
						v2(0.0f,-120.0f)
					})
				}),
				Hitbox({
					Triangle({
						v2(0.0f,0.0f),
						v2(50.0f,-100.0f),
						v2(-50.0f,-100.0f)
					}),
					Triangle({
						v2(50.0f,-100.0f),
						v2(-50.0f,-100.0f),
						v2(0.0f,-120.0f)
					})
				}),
				Hitbox({
					Triangle({
						v2(0.0f,0.0f),
						v2(75.0f,-125.0f),
						v2(-75.0f,-125.0f)
					}),
					Triangle({
						v2(75.0f,-125.0f),
						v2(-75.0f,-125.0f),
						v2(0.0f,-160.0f)
					})
				})
			});
	break;
	}
	#undef v2
	timers.resize(3);
	for (int i = 0; i < 3; i++) {timers.push_back(thor::Timer());}
}

// http://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect
// send help, this is horrifying

// Returns 1 if the lines intersect, otherwise 0. In addition, if the lines 
// intersect the intersection point may be stored in the floats i_x and i_y.
bool get_line_intersection(sf::Vector2f p0, sf::Vector2f p1, sf::Vector2f p2, sf::Vector2f p3, float *i_x, float *i_y){
	float p0_x = p0.x;      float p0_y = p0.y;
	float p1_x = p1.x;      float p1_y = p1.y;
	float p2_x = p2.x;      float p2_y = p2.y;
	float p3_x = p3.x;      float p3_y = p3.y;

    float s1_x, s1_y, s2_x, s2_y;
    s1_x = p1_x - p0_x;     s1_y = p1_y - p0_y;
    s2_x = p3_x - p2_x;     s2_y = p3_y - p2_y;

    float s, t;
    s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) / (-s2_x * s1_y + s1_x * s2_y);
    t = ( s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) / (-s2_x * s1_y + s1_x * s2_y);

    if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
    {
        // Collision detected
        if (i_x != NULL)
            *i_x = p0_x + (t * s1_x);
        if (i_y != NULL)
            *i_y = p0_y + (t * s1_y);
        return true;
    }

    return false; // No collision
}

sf::Vector2f rotateAround(sf::Vector2f pivot, sf::Vector2f point, float angle){
	// works somehow, trial and error and pain
	// please don't change this unless you KNOW what you're doing -1/27
	// might have to rotate fixed 90 degrees afterwards though
	const float s =  std::sin(angle);
	const float c =  std::cos(angle);
	point         -= pivot;
	double xnew   =  (c * point.x) - (s * point.y);
	double ynew   =  (s * point.x) + (c * point.y);
	return sf::Vector2f(xnew + pivot.x, ynew + pivot.y);
}

void rotateTriangle(sf::Vector2f pivot, Triangle& out_triangle, float angle){
	out_triangle[0] = rotateAround(pivot, out_triangle[0], angle);
	out_triangle[1] = rotateAround(pivot, out_triangle[1], angle);
	out_triangle[2] = rotateAround(pivot, out_triangle[2], angle);
}

bool triRectCollisionCheck(Triangle tri, sf::FloatRect frect){
	bool isHit = false;
	
	std::vector<sf::Vector2f> rect;
	rect.push_back(sf::Vector2f(frect.left, frect.top));
	rect.push_back(sf::Vector2f(frect.left + frect.width, frect.top));
	rect.push_back(sf::Vector2f(frect.left + frect.width, frect.top + frect.height));
	rect.push_back(sf::Vector2f(frect.left, frect.top + frect.height));
	
	// Iterate through each rectangle's side, compare each triangle line to it - 
	// if a match is found, just break, return true (if none is found return false)
	typedef sf::Vector2f Line[2];
	for (int rectSideIdx = 0; rectSideIdx < 4; rectSideIdx++){
		for (int triSideIdx = 0; triSideIdx < 3; triSideIdx++){
			Line l1 = {rect[rectSideIdx] + rect[rectSideIdx%4]};
			Line l2 = {tri[triSideIdx] + tri[triSideIdx%3]};
			float x, y;
			if (get_line_intersection(l1[0], l1[1], l2[0], l2[1], &x, &y)) isHit = true;
			if (isHit) break;
		}
		if (isHit) break;
	}
	return isHit;
}

// state must be named state for awful macros to work
// b a d   
// i d e a 
// the hardcoded macro names are straight up horrible
void checkSwing(GameState* state, Weapon* w, float angle, sf::Vector2f origin, std::string attack, bool isPlayer){
	//Check collision, raycast backwards to not swing through walls (eventually)
	// TODO: add raycasting
	if (isPlayer){
		std::vector<Collision*> colliders;
		std::vector<Enemy*> enemies;
		if (gCTEST(enemies) && gCTEST(collisions)){
			for (auto ent : entities(gC(enemies), gC(collisions))){
				Enemy& e     = get<Enemy>(ent);
				Collision& c = get<Collision>(ent);
				colliders.push_back(&c);
				enemies.push_back(&e);
			}
		}

		if (attack == "swing"){
			std::vector<int> hitEnemiesIds;
			HitboxSeries hs = w->data.hitboxes["swing"];
			Hitbox* cHitbox = &hs[w->data.cSwingCount];
			
			for (auto& tri : *cHitbox){
				for (auto& pt : tri){
					pt += origin;
				}
			}

			// DEBUG: draw colliders for testing
			for (auto& tri : (*cHitbox)){	
				sf::VertexArray VT(sf::LinesStrip, 4);

				// This creates aligned circles showing rotatable axes.
				// Great for debug.

				// sf::CircleShape cs;
				// cs.setFillColor(sf::Color::Transparent);
				// cs.setOutlineThickness(1);
				// int idx = 0; for (auto pt : tri){
				// 	cs.setRadius(std::sqrt(std::abs(std::pow(pt.y-origin.y, 2)) + std::abs(std::pow(pt.x-origin.x, 2))));
				// 	cs.setPosition(origin-sf::Vector2f(cs.getRadius(),cs.getRadius()));
				// 	switch(idx%3){
				// 		case 0: cs.setOutlineColor(sf::Color::Red);   break;
				// 		case 1: cs.setOutlineColor(sf::Color::Green); break;
				// 		case 2: cs.setOutlineColor(sf::Color::Blue);  break;
				// 	}
				// 	state->draw_circles.push_back(cs);
				// 	++idx;
				// }

				rotateTriangle(origin, tri, angle);

				VT[0].position = tri[0]; VT[0].color = sf::Color::Red;
				VT[1].position = tri[1]; VT[1].color = sf::Color::Red;
				VT[2].position = tri[2]; VT[2].color = sf::Color::Red;
				VT[3].position = tri[0]; VT[3].color = sf::Color::Red; // connect that last piece
				state->drawables.push_back(VT);
			}

			// Take each collider in hitbox and collisioncheck, find those hit
			for (auto& tri : (*cHitbox)){
				for (auto& collider : colliders) {
					if (triRectCollisionCheck(tri, collider->rs[0])){
						hitEnemiesIds.push_back(collider->Id);
					}
				}
			}

			// remove duplicates - no enemy hit twice
			std::set<int> s( hitEnemiesIds.begin(), hitEnemiesIds.end() );
			hitEnemiesIds.assign( s.begin(), s.end() );

			// finally hit them
			for (int& id : hitEnemiesIds){
				for (auto& en : enemies){
					Engine::Console()->info("hit an enemy lmao");
					if (en->id() == id) en->s.applyDamage(w->damage.roll());
				}
			}
		}
	}
}

SwingResult Weapon::swing(GameState* state, float angle, sf::Vector2f origin, bool isPlayer){
	timers[(int)TimerNames::Between].stop();
	if (isOnCooldown){
		if (timers[(int)TimerNames::Cooldown].isExpired()){
			isWoundUp = false;
			isWindingUp = false;
			isOnCooldown = false;
		}
		else {
			lastSwingResult = SwingResult::InCooldown;
			return SwingResult::InCooldown;
		}
	}
	if (!isWindingUp && !isOnCooldown){
		isWindingUp = true;
		if (data.windups[data.cSwingCount] < 0.02f) {
			isWoundUp = true;
			isWindingUp = false;
		}
		else {
			isWoundUp = false;
			timers[(int)TimerNames::Windup].restart(sf::seconds(data.windups[data.cSwingCount]));
		}
	}
	if (isWindingUp && !isOnCooldown){
		if (timers[(int)TimerNames::Windup].isExpired()){
			isWoundUp = true;
			isWindingUp = false;
		}
		else {
			lastSwingResult = SwingResult::InWindup;
			return SwingResult::InWindup;
		}
	}
	if (isWoundUp && !isOnCooldown){
		checkSwing(state, this, angle, origin, "swing", isPlayer);
		isWoundUp = false;
		isOnCooldown = true;
		timers[(int)TimerNames::Cooldown].restart(sf::seconds(data.cooldowns[data.cSwingCount] ));
		timers[(int)TimerNames::Between ].restart(sf::seconds(data.cooldowns[data.cSwingCount] + data.swingInterrupt));
		data.cSwingCount = (++data.cSwingCount)%(data.swingCount);
		lastSwingResult = SwingResult::Completed;
		return SwingResult::Completed;
	}
	// Control can't get to here. still should return but no option is good
}

void Weapon::update(){
	if (timers[(int)TimerNames::Between].isExpired()){
		data.cSwingCount = 0;
	}
}