#pragma once
#include "Engine/Components.h"
#include "States/Game/Item.h"

enum class GMTState {
	Undiscovered,
	Discovered,
	Current,
	Cleared,
	Itemleft,
	Boss
};
struct GameModel {
	bool unlockedCamera = false;
	int c_level = 1;
	bool firstlevel = true;
	Stats initstats;
	// default stats provided from menu
	GameModel(std::string type) {
		if (type == "fighter")
		{
			initstats.chp = 20;
			initstats.cmp = 10;
			initstats.str = 5;
			initstats.dex = 5;
			initstats.int_= 5;
			initstats.mhp = 20;
			initstats.mmp = 10;
			initstats.ms = 100;
		}
	}
	std::map<sf::Vector2i, GMTState> tileStates;
	Stats _tplayerStats;
	Inventory _tplayerInventory;
};