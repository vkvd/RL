#pragma once

// its ugly but wow is this a lifesaver
// not todo: don't refactor this

//  ########
//  #      #
//  # @ -> +
//  #      #
//  ########
enum class EntryDir {
	Up       = 0,
	Left     = 1,
	Right    = 2,
	Down     = 3,
	Centered = 4
};

//  ########
//  #      #
//  +@ ->  #
//  #      #
//  ########
enum class ExitDir {
	Teleport = 4,
	Up       = 3,
	Left     = 2,
	Right    = 1,
	Down     = 0
};

static_assert((int)EntryDir::Up    == (int)ExitDir::Down,  "Doors are broken");
static_assert((int)EntryDir::Down  == (int)ExitDir::Up,    "Doors are broken");
static_assert((int)EntryDir::Right == (int)ExitDir::Left,  "Doors are broken");
static_assert((int)EntryDir::Left  == (int)ExitDir::Right, "Doors are broken");


static ExitDir EntryToExit(EntryDir entry) {
	return static_cast<ExitDir>((int)entry);
}

static EntryDir ExitToEntry(ExitDir exit) {
	return static_cast<EntryDir>((int)exit);
}

static EntryDir ToEntryDir(int x) {
	return static_cast<EntryDir>(x);
}

static ExitDir ToExitDir(int x) {
	return static_cast<ExitDir>(x);
}