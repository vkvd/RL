#pragma once
#include <string>
#include "Engine/Random.h"
#include "Engine/Engine.h"
#include "States/Game/Item.h"


/*
// "the balance file"
// 
// contains things relating to enemies and what they do
// pretty much a giant block of constants that should be
// relegated to json but it works ¯\_(ツ)_/¯
*/


// Enemy types
enum class EnemyClass {
	Fighter,
	Ranged
};

// The AI states
enum class AIState {
	Idle,
	IdleWalk,
	Approach,
	Chase,
	Retreat,
	Circle,
	OnAttack,
	OnTakeDamage
};

// The main things an enemy needs
struct EnemyData {
	EnemyClass eclass;
	AIState behavior;
};

// very helpful with the prefabs
// base enemy prefab creates sprite, this is basically an 
// argument pack
struct EnemySpriteData {
	EnemySpriteData(){}
	EnemySpriteData(sf::Texture* t, int w, int h) : texture(t), width(w), height(h) {}
	sf::Texture* texture;
	bool animated = true;
	int width;
	int height;
	int offsetx = 0;
	int offsety = 0;
};


// A singleton per enemy type
// This isn't the best workaround but it exists
// it's basically a bunch of constants organized by name

namespace EnemyVars {
	class Fighter {
	public:
		// Stats
		int hp                  = 10;
		int str                 = 5;
		int dex                 = 5;
		int int_                = 5;
		int ms                  = 300;
		
		// AI Config
		int AInoticeRangeRadius = 300;
		
		// Sprite data
		// !Initialized after engine!
		EnemySpriteData spritedata;
		
		// Randomgen bounds
		// !Initialized after engine!
		Random _swingDelayPreMult;
		float getSwingDelay() {
			return (_swingDelayPreMult.roll()*0.1f);
		}
		Weapon rollViableWeapon() {
			int x = Engine::RNG->roll(1,3);
			int y = Engine::RNG->roll(x,3);
			return Weapon(WeaponType::Sword, Engine::RNG->createRandom(Engine::RNG->_seed, x, y, 0));
		} 
		static Fighter& Data(){
			static Fighter f;
			return f;
		}

	private:
		Fighter(){}
		Fighter& operator=(const Fighter&);
		Fighter(const Fighter&);
	};

	// Things that reference engine statics
	// if it has to be done after engine init put it here
	static void initEnemyVars(){
		Fighter::Data().spritedata = EnemySpriteData(
			&(*Engine::Textures)["fighter"],
			64, // width
			64  // height
		);
		Fighter::Data()._swingDelayPreMult = Engine::RNG->createRandom(Engine::RNG->_seed, 0, 3, 1);
	}
}