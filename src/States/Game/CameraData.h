#pragma once
#include "SFML/Graphics.hpp"
struct CameraData {
	CameraData(sf::Vector2f ws) :
		window(ws),
		position(sf::Vector2f(0, 0)),
		center(sf::Vector2f(window.x / 2, window.y / 2)),
		zoom(1.0f) 
	{}
	void apply(sf::View& view) {
		view.reset(sf::FloatRect(0, 0, window.x, window.y));
		view.setCenter((sf::Vector2f)center);
		view.zoom(zoom);
	}
	void updateCenter() {
		center = (sf::Vector2i(window.x / 2, window.y / 2))+position;
	}
	void updatePosition() {
		position = -(sf::Vector2i(window.x / 2, window.y / 2)) + center;
	}
	void zoomIn() {
		zoom *= 1 + zoomScale;
	}
	void zoomOut() {
		zoom *= 1 - zoomScale;
	}
	sf::Vector2f window;
	sf::Vector2i position;
	sf::Vector2i center;
	float zoom;
	float zoomScale = 0.1f;
};