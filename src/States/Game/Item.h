#pragma once
#include "Engine/Random.h"
#include "SFML/Graphics.hpp"
#include "Thor/Time.hpp"

enum class WeaponType {
	Axe,
	Sword,
	Spear
};

const int _timerNamesSize = 3;
enum class TimerNames {
	Windup,
	Cooldown,
	Between
};

enum class SwingResult {
	InWindup,
	Completed,
	InCooldown
};

// TODO: hitboxes... really confusing
typedef std::vector<sf::Vector2f> Triangle;
typedef std::vector<Triangle> Hitbox;
typedef std::vector<Hitbox> HitboxSeries;
typedef std::map<std::string, HitboxSeries> Hitboxes;

struct SwingData {
	Hitboxes hitboxes;
	int swingCount;
	std::vector<float> damageMultipliers;
	std::vector<float> windups;
	std::vector<float> cooldowns;
	float swingInterrupt;
	int cSwingCount = 0;
	bool isSwinging = false;
};
class Item {
public:
	Item();
	~Item();
};
class GameState;
class Weapon : public Item {
public:
	Weapon();
	Weapon(WeaponType type, Random damage);
	~Weapon();
	SwingResult swing(GameState* state, float angle, sf::Vector2f origin, bool isPlayer);
	Random damage;
	SwingData data;
	WeaponType type;
	std::vector<thor::Timer> timers;
	void update();
	bool isWoundUp = false; 
	bool isWindingUp = false;
	bool isOnCooldown = false;
	SwingResult lastSwingResult = SwingResult::Completed;
	// TODO: elemental damage? some time in the future
};