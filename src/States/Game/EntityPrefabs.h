#pragma once
#include "Engine/Components.h"
#include "Engine/AnimationData.h"

class TileMap;
class GameState;
class GameMap;

namespace {
	void _map(GameState* state, TileMap* map);
}

namespace Prefabs {
	// notation: {prefab call}: [{opt prefab calls}] => (component calls)

	// {player}: (positions, sprites, players, velocities, collisions)
	int player(GameState* state);
	// {player}: (positions, sprites, players, velocities, collisions)
	int player(GameState* state, sf::Vector2f pos, Stats s);
	int player(GameState* state, sf::Vector2f pos, Stats s, Inventory i);
	// {exit}: {sprite} + (collider + exit)
	int exit(GameState* state, bool active, int x, int y, sf::Texture* t, int tw, int th, sf::Vector2i target);
	// {wall}: {sprite noanim} and (collider) => (position, sprite, collider) with tilesheet slice
	int wall(GameState* state, int x, int y, int tw, int th, WallPos p, sf::Texture* t);
	// {sprite}: noanim (sprite, position)
	int sprite(GameState* state, int x, int y, sf::Texture* t, bool animated, int width, int height, int ox, int oy);
	// {sprite}: anim (sprite, position)
	int sprite(GameState* state, int x, int y, sf::Texture* t, bool animated, int width, int height, AnimationData data, int ox, int oy);
	// {enemy}: --iden to player with player ->enemy
	// shell for other ones
	int enemy(GameState* state, sf::Vector2f pos, EnemySpriteData esd, AnimationData anim, EnemyData d, Stats s, Inventory i, Pathfinding pf) ;	
	// --Specialized prefabs
	int fighter(GameState* state, sf::Vector2f pos, float scaling);
	// {rectangle}: 
	int rectangle(GameState* state, sf::FloatRect rectangle, sf::Color color);




	void map(GameState* state, GameMap* gamemap, sf::Vector2i loc);
	void gamemap(GameState* state, GameMap* map, int w, int h, std::string roomlist);
}