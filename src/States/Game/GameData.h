#pragma once
#include "ECS/ecs.h"
#include "Engine/Components.h"
struct GameData {
	// Entities with a location somewhere around the map
	component_set<Position> positions;
	// Entities that move or can move
	component_set<Velocity> velocities;
	// Entities with drawable sprites
	component_set<Sprite> sprites;
	// Entity(possibly more??) marked as player (multiplayer?)
	component_set<Player> players;
	// Entities marked as enemies
	component_set<Enemy> enemies;
	// Entities that collide with things or something
	component_set<Collision> collisions;
	// Entities that are exits... lol. gotta have the set anyway
	component_set<Exit> exits;
};