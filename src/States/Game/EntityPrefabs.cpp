#include "EntityPrefabs.h"
#include "Systems/Systems.h"
#include "States/GameState.h"
#include "Tilemap/GameMap.h"
#include "States/LoadState.h"
using namespace ecs;

namespace Prefabs {
	int player(GameState* state) {
		Stats s;
		s.chp  = 0;
		s.cmp  = 0;
		s.dex  = 0;
		s.int_ = 0;
		s.mhp  = 0;
		s.mmp  = 0;
		s.ms   = 0;
		s.str  = 0;
		Buffs    b;
		s.b    = b;
		s.b.modifiers.clear();
		s.b.timers.clear();
		player(state, sf::Vector2f(0,0),s);
		return -1;
	}
	int player(GameState* state, sf::Vector2f pos, Stats s) {
		return player(state, pos, s, Inventory());
	}
	int player(GameState* state, sf::Vector2f pos, Stats s, Inventory i) {
		Engine::Console()->debug("Creating player from prefab.");
		int id = state->newEntity();

		AnimNames names = { "idle", "move" };
		Anims frames = {
			{ { 0,0 } }, // idle is 0,0
			{ { 0,0 },{ 1,0 },{ 2,0 },{ 3,0 } } // walk is horizontal
		};
		Times lengths = { 0.5f, 0.5f }; // half second for anim

		AnimationData anim;
		anim.addAnimations(names, frames, lengths);

		state->positions.state->emplace(id, pos.x, pos.y);
		state->sprites.state->emplace(
			id, 
			&(*Engine::Textures)["player"], 
			true, 
			64, 
			64, 
			anim, 
			0, 
			0
			);
		state->players.state->emplace(id, s);
		state->velocities.state->emplace(id, 0, 0);
		state->collisions.state->emplace(id, pos.x, pos.y, 64, 64, false);

		Inventory inv;
		inv.weapons.push_back(Weapon());
		inv.equippedWeapon = &inv.weapons[0];

		entity<Player> p            = *(entities_find(id, *(state->players.state)));
		Player& playerdata          = get<Player>(p);
		playerdata.s                = s;
		playerdata.i                = inv;
		playerdata.i.equippedWeapon = &playerdata.i.weapons[0];

		Systems::singlePassSetSpriteTextures(state);
		return id;
	}
	int exit(GameState* state, bool active, int x, int y, sf::Texture* t, int tw, int th, sf::Vector2i target) {
		Engine::Console()->debug("Creating exit from prefab.");
		int id = sprite(state, x, y, t, false, tw, th, 0, 0);
		state->exits.state->emplace(id, active, target);

		// Door margin to sink door collider into the wall with a 1px margin;
		// stops people getting sucked into exits
		const int dm = 3;
		const int cx = x  + dm;
		const int cw = tw - dm * 2;
		const int cy = y  + dm;
		const int ch = th - dm * 2;

		// Engine::Console()->info("x:  {} | y:  {}", x, y);
		// Engine::Console()->info("cx: {} | cy: {}", cx, cy);
		// Engine::Console()->info("w:  {} | h:  {}", tw, th);
		// Engine::Console()->info("cw: {} | ch: {}", cw, ch);

		state->collisions.state->emplace(id, cx, cy, cw, ch, true);
		//state->sprites.emplace(id, t, false, tw, th);
		return id;
		Systems::singlePassSetSpriteTextures(state);
	}
	int wall(GameState* state, int x, int y, int tw, int th, WallPos p, sf::Texture* t) {
		Engine::Console()->debug("Creating wall from prefab.");
		int id             = sprite(state, x, y, t, false, tw, th, 0, 0);
		int tcoordx        = (int)p % 4;
		int tcoordy        = std::floor((int)p / 4);
		entity<Sprite> ent = *(entities_find(id, *(state->sprites.state)));
		Sprite& s          = get<Sprite>(ent);
		s.sprite.setTexture(*s.texture);
		s.sprite.setTextureRect(sf::IntRect(tcoordx*tw, tcoordy*th, tw, th));
		//Engine::Console()->info("x: {} y: {}", x, y);
		state->collisions.state->emplace(id, x, y, tw, th, true);
		Systems::singlePassSetSpriteTextures(state);
		return id;
	}
	int sprite(GameState* state, int x, int y, sf::Texture* t, bool animated, int width, int height, int ox = 0, int oy = 0) {
		Engine::Console()->debug("Creating sprite (no animation) from prefab.");
		int id = state->newEntity();
		state->positions.state->emplace(id, x, y);
		state->sprites.state->emplace(id, t, animated, width, height, ox, oy);
		return id;
	}
	int sprite(GameState* state, int x, int y, sf::Texture* t, bool animated, int width, int height, AnimationData data, int ox = 0, int oy = 0) {
		Engine::Console()->debug("Creating sprite (with animation) from prefab.");
		int id = state->newEntity();
		state->positions.state->emplace(id, x, y);
		state->sprites.state->emplace(id, t, animated, width, height, data, ox, oy);
		return id;
	}
	// Toplevel mapgenerator. Should be boardsize-additionals later. Loads internal json?
	void map(GameState* state, GameMap* gamemap, sf::Vector2i loc) {
		Engine::Console()->debug("Creating tilemap from prefab.");
		if (state->gamemap.map[loc].mapID != 0){
			TileMap map;
			for (auto it = gamemap->spritelist.begin(); it != gamemap->spritelist.end(); ++it) {
				std::vector<sf::Texture*> textures;
				for (auto s : it->second) {
					textures.push_back(&(*Engine::Textures)[s]);
				}
				map.addSprites(it->first, textures);
			}
			map.h     = gamemap->map[loc].data.h;
			map.w     = gamemap->map[loc].data.w;
			map.th    = gamemap->map[loc].data.th;
			map.tw    = gamemap->map[loc].data.tw;
			map.tiles = gamemap->map[loc].data.tiles;
			map.createPrefabs(state, map.tw, map.th);
			_map(state, &map);
		}
	}
	void gamemap(GameState * state, GameMap * map, int w, int h, std::string roomlist)
	{
		state->isMapInitialized = false;
		Engine::drawFreeze      = true;
		float progress          = 0.0f;
		LoadState* ls           = new LoadState(progress);
		ls->enter();
		// Create loading screen and update it with w/h/percents... draw is %based  no threads
		Engine::Console()->debug("Creating full gamemap from prefab.\n\tCreating loading screen...");
		map->loadJSON(roomlist);
		map->generate(state, w, h);
		for (int y = 0; y < h; y++) {
			for (int x = 0; x < w; x++) {
				state->gamemapPos = sf::Vector2i(x, y);
				state->updateComponentPointers();
				Prefabs::map(state, map, state->gamemapPos);
				state->model.tileStates[sf::Vector2i(x,y)] = GMTState::Undiscovered;
				// this is probably somewhat wrong? it works though
				// TODO: understand this?
				progress = (float)((y)*w+(x+1))/(float)((w)*(h));
				Engine::Console()->debug("xy {} {} Progress {}", x, y, progress);
				Engine::Window->clear();
				ls->update();
				ls->draw();
				Engine::Window->display();
			}
		}
		ls->exit();
		map->setSpawn(state);
		Engine::Console()->info("Spawn?{} {} {} {}", state->gamemapPos.x, state->gamemapPos.y, map->spawn.x, map->spawn.y);
		Engine::drawFreeze = false;
		state->isMapInitialized = true;
	}
	int enemy(GameState* state, sf::Vector2f pos, EnemySpriteData esd, AnimationData anim, EnemyData d, Stats s, Inventory i, Pathfinding pf) {
		Engine::Console()->info("Creating enemy from prefab.");
		int id = state->newEntity();
		state->positions.state->emplace(id, pos.x, pos.y);
		state->sprites.state->emplace(
			id, 
			esd.texture, 
			esd.animated, 
			esd.width, 
			esd.height, 
			anim, 
			esd.offsetx, 
			esd.offsety
			);
		state->enemies.state->emplace(id, d, s, i, pf);
		state->velocities.state->emplace(id, 0, 0);
		state->collisions.state->emplace(id, pos.x, pos.y, 64, 64, false);
		entity<Enemy, Sprite> ent = *(entities_find(id, *(state->enemies.state), *(state->sprites.state)));
		Enemy& enemy = get<Enemy>(ent);
		Sprite& spr = get<Sprite>(ent);
		enemy.i.equippedWeapon = &enemy.i.weapons[0];
		enemy.pf.state = state;

		spr.sprite.setTexture(*spr.texture);
		return id;
	}
	// --Specialized prefabs
	int fighter(GameState* state, sf::Vector2f pos, float scaling) {
		// TODO: scaling
		EnemyData d;
		Stats s;
		Inventory i;
		Pathfinding pf(state);
		s.chp      = EnemyVars::Fighter::Data().hp;
		s.mhp      = EnemyVars::Fighter::Data().hp;
		s.str      = EnemyVars::Fighter::Data().str;
		s.dex      = EnemyVars::Fighter::Data().dex;
		s.int_     = EnemyVars::Fighter::Data().int_; 
		s.ms       = EnemyVars::Fighter::Data().ms;
		d.behavior = AIState::Idle;
		d.eclass   = EnemyClass::Fighter;
		Weapon ew  = EnemyVars::Fighter::Data().rollViableWeapon();
		i.weapons.push_back(ew);

		AnimNames names;
		Anims frames; 
		names = { "idle", "move" };
		frames  = {
			{ { 0,0 } }, // idle is 0,0
			{ { 0,0 },{ 1,0 },{ 2,0 },{ 3,0 } } // walk is horizontal
		};
		Times animlengths = {0.5f, 0.5f};
		AnimationData anim;
		anim.addAnimations(names, frames, animlengths);
		return Prefabs::enemy(state, pos, EnemyVars::Fighter::Data().spritedata, anim, d, s, i, pf);
	}

	int rectangle(GameState* state, sf::FloatRect rectangle, sf::Color color){
		Engine::Console()->debug("Creating rectangle from prefab.");
		int id = state->newEntity();
		state->rectangles.state->emplace(id, rectangle, color);
		return id;
	}
}

// File specific internal use functions
// this is here to jam it at the end of the file but still be 
// too lazy to forward declare it
namespace {
	void _map(GameState* state, TileMap* map) {
		Systems::singlePassSetSpriteTextures(state);
	}
}
