#pragma once
#include "Engine/State.h"
#include "Thor/Input.hpp"
#include "Thor/Time.hpp"
#include "Engine/Engine.h"
#include "Tilemap/Tile.h"
#include "ECS/ecs.h"
#include "Engine/Components.h"
#include "States/Game/CameraData.h"
#include "States/Game/GameModel.h"
#include "Tilemap/GameMap.h"
#include "json/json.hpp"
#include <iostream>
#include <fstream>


template<typename T>
struct CompList {
	// 2D array that matches component lists to map positions
	std::map<sf::Vector2i, component_set<T>> map;
	// Points to element of ^map (a component set) with what the 
	// game currently needs to work.
	component_set<T>* state;
	// size: for clearing
	sf::Vector2i _size;

	// Repoint state to map[gamemappos] when we change map
	void updatePointer(sf::Vector2i gmp) {
		state = &(map[gmp]);
	}
	
	// reserve the sets in a map of size (size) with elements (s)
	CompList(sf::Vector2i size, size_t s) {
		_size=size;
		for (int j = 0; j < size.y; j++) {
			for (int i = 0; i < size.x; i++) {
				static_cast<component_set<T>>(map[sf::Vector2i(i, j)]).reserve(1000);
			}
		}
		updatePointer(sf::Vector2i(0, 0));
	}
	// default: just reserve a big map
	CompList() {
		_size=sf::Vector2i(10,10);
		CompList(sf::Vector2i(10, 10), 2000);
	}
	// Map of entity -> location is handled by gamestate, therefore requires GS' ent map to delete
	void erase(std::map<int, sf::Vector2i>* em, int ent) {
		if (map[(*em)[ent]].find(ent) != map[(*em)[ent]].end()) { 
			map[(*em)[ent]].erase(ent); 
		}
	}
	void clear(){
		for (int y = 0; y < _size.y; y++){
			for (int x = 0; x < _size.x; y++){
				map[sf::Vector2i(x,y)].clear();
			}
		}
	}
};

class GameState : public State
{
public:
	// Constructors and deconstructors
	GameState();
	GameState(GameModel model);
	~GameState();

	// Standard state functions
	void enter();
	void loadContent();
	float dt = 0.0f; // dt for access from components;
	void update(float _dt);
	void draw();
	void exit();
	void gameOver();
	// Toplevel full map creation.
	// Creates a gamemap and populates it full of tilemaps and mapdata.
	void newMap(int w, int h);
	void createLoadingScreen(float& progress);

	// Minimap functions
	sf::Vector2i screenToWorld(sf::Vector2i mpos);
	sf::Vector2i mouseToTile(sf::Vector2f worldPos, int th, int tw);

	// Model containing player stats, buffs, inventory...
	GameModel model;

	// Views
	CameraData cameraData;
	sf::View camera;
	sf::View minimap;

	// Gamemap 
	GameMap gamemap;
	sf::Vector2i gamemapPos;
	bool isMapInitialized = false;

	// ECS helpers
	int playerID = 0;

	// Entities

	// Entities with a location somewhere around the map

	CompList<Position> positions;
	// Entities that move or can move
	CompList<Velocity> velocities;
	// Entities with drawable sprites
	CompList<Sprite> sprites;
	// Entity(possibly more??) marked as player (multiplayer?)
	CompList<Player> players;
	// Entities marked as enemies
	CompList<Enemy> enemies;
	// Entities that collide with things or something
	CompList<Collision> collisions;
	// Entities that are exits... gotta have the set anyway. it just works?
	CompList<Exit> exits;
	// Simple rectangle drawing game-world objects. Camera tracking small popups, etc.
	CompList<Rectangle> rectangles;
	// Simple world based text objects.
	//CompList<Text> texts

	std::vector<sf::VertexArray> drawables;
	std::vector<sf::CircleShape> draw_circles;
	std::vector<UIText> ui_texts;

	std::vector<thor::CallbackTimer> timers;

	void clearEntities(sf::Vector2i size);
	void updateComponentPointers();
	int c_ent = 1;
	int newEntity();
	void deleteEntity(int ent);
	std::map<int, sf::Vector2i> ent_map;
	std::vector<int> f_ents;
};

