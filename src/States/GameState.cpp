#include "GameState.h"
//#include "tinydir.h"
#include <iostream>
#include "Systems/Systems.h"
#include "States/Game/EntityPrefabs.h"
#include "States/Game/ContentList.h"
#include "States/LoadState.h"
#include "States/Game/DoorDirections.h"
#include "Thor/Input.hpp"
#include "States/Game/EnemyData.h"

GameState::GameState() : model(GameModel("fighter")), cameraData((sf::Vector2f)WINDOWSIZE)
{
	u_passthrough = false;
	d_passthrough = false;
}

GameState::GameState(GameModel model) :  model(model), cameraData((sf::Vector2f)WINDOWSIZE)
{
	u_passthrough = false;
	d_passthrough = false;
}

GameState::~GameState()
{
	
}

void GameState::loadContent() {

	//Engine::Shaders->acquire("glow", THORLOAD(sf::Shader, "content/shaders/glow.frag"));
	// Load content here :^)

	Engine::Console()->info("Starting to load content...");

	// J S O N 
	// L O A D I N G
	// F O R M A T

	// { "key":"path" }
	// that's it

    using json = nlohmann::json;
    json data; 
    std::ifstream stream;
    
    stream.open("content/entities/entities.json", std::ios::in); data << stream;
    for (json::iterator it = data.begin(); it != data.end(); ++it){
    	TEXLOAD(it.key(), "content/entities/"+it.value().get<std::string>());
    }

    data.clear(); stream.close();
    stream.open("content/ui/ui.json", std::ios::in); data << stream;
    for (json::iterator it = data.begin(); it != data.end(); ++it){
    	TEXLOAD(it.key(), "content/ui/"+it.value().get<std::string>());
    }

    data.clear(); stream.close();
    stream.open("content/fonts/fonts.json", std::ios::in); data << stream;
    for (json::iterator it = data.begin(); it != data.end(); ++it){
    	FNTLOAD(it.key(), "content/fonts/"+it.value().get<std::string>());
    }

    // After the engine has initialized, then set up enemy globals
    EnemyVars::initEnemyVars();
    PFNode::state = this;

	Engine::Console()->info("Finished loading content.");
	
	model = GameModel("fighter");
}

void GameState::enter()
{
	loadContent();
	newMap(5, 5);
}

void GameState::update(float _dt) {
	dt = _dt;
	if (Engine::Input->isActive(Actions::dbgNewMap)) {
		newMap(5, 5);
	}
	
	Systems::update(this);
}

void GameState::draw()
{
	if (isMapInitialized) {
		cameraData.apply(camera);
		camera.zoom(cameraData.zoom);
		Engine::Window->setView(camera);
		Systems::draw(this);
	}
}


void GameState::exit()
{
}


sf::Vector2i GameState::screenToWorld(sf::Vector2i mpos)
{
	mpos += sf::Vector2i(cameraData.position);

	// TODO: zoom?
	return mpos;
}

sf::Vector2i GameState::mouseToTile(sf::Vector2f worldPos, int th, int tw)
{
	return sf::Vector2i(std::floor(worldPos.x / tw), std::floor(worldPos.y / th) );
}

void GameState::clearEntities(sf::Vector2i size)
{
	for (int y = 0; y < size.y; y++){
		for (int x = 0; x < size.x; x++){
			gamemapPos = sf::Vector2i(x,y);
			updateComponentPointers();
			positions.state  ->clear();			
			velocities.state ->clear();			
			sprites.state    ->clear();			
			players.state    ->clear();			
			enemies.state    ->clear();			
			collisions.state ->clear();
			exits.state      ->clear();
			positions._size  = sf::Vector2i(x,y);
			velocities._size = sf::Vector2i(x,y);
			sprites._size    = sf::Vector2i(x,y);
			players._size    = sf::Vector2i(x,y);
			enemies._size    = sf::Vector2i(x,y);
			collisions._size = sf::Vector2i(x,y);
		}
	}
}

void GameState::updateComponentPointers() {
	positions.updatePointer(gamemapPos);
	velocities.updatePointer(gamemapPos);
	sprites.updatePointer(gamemapPos);
	players.updatePointer(gamemapPos);
	enemies.updatePointer(gamemapPos);
	collisions.updatePointer(gamemapPos);
	exits.updatePointer(gamemapPos);
	//vertexarrays.updatePointer(gamemapPos);
}

int GameState::newEntity()
{
	if (f_ents.size() == 0) {
		c_ent++;
		ent_map[c_ent] = gamemapPos;
		return c_ent;
	}
	else {
		int r = f_ents.back();
		ent_map[r] = gamemapPos;
		f_ents.pop_back();
		return r;
	}
	return 0;
}

void GameState::deleteEntity(int ent)
{
	Engine::Console()->info("Deleting and freeing entity... id: {}", ent);
	positions.erase(&ent_map, ent);
	velocities.erase(&ent_map, ent);
	sprites.erase(&ent_map, ent);
	players.erase(&ent_map, ent);
	enemies.erase(&ent_map, ent);
	collisions.erase(&ent_map, ent);
	exits.erase(&ent_map, ent);
	f_ents.push_back(ent);
	Engine::Console()->info("Free entities: {}", f_ents.size());

}

void GameState::newMap(int w, int h)
{
	Engine::InputBlocker = true;
	Engine::drawFreeze = true;
	isMapInitialized = false;
	sf::Vector2i size(w, h);
	clearEntities(size);
	updateComponentPointers();
	gamemap = GameMap();
	Prefabs::gamemap(this, &gamemap, w, h, "content/rooms/rooms.json");
	Systems::makeRoom(this, EntryDir::Centered);
	isMapInitialized = true;
	// Set camera by running player controller before drawing -- screen tearing fix
	Engine::Window->clear();
	Systems::playerController(this);
	Systems::drawTextures(this);
	Engine::Window->display();
	Engine::drawFreeze = false;
	Engine::InputBlocker = false;
}

void GameState::createLoadingScreen(float & progress)
{
	LoadState ls(progress);
}

void GameState::gameOver(){

}