#pragma once
#include "Engine/State.h"
#include "Engine/Engine.h"

class LoadState
{
public:
	LoadState(float& progress);
	void enter();
	void loadContent();
	void update();
	void draw();
	void exit();
	float& progress;
	
	sf::RectangleShape background;
	sf::RectangleShape loadBorder;
	sf::RectangleShape loadProgress;
	sf::Text loadingText;
	
	float chunksize = 10.0;
	float barWidth = 4; // in chunks
	float barHeight = 1;
	sf::Vector2f loadBorderSize = sf::Vector2f(4,4);
};

