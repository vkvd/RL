#pragma once
#include <map>
#include <string>
#include "SFML/Graphics.hpp"
class State
{
public:
	virtual void enter() = 0;
	virtual void update(float dt) = 0;
	virtual void draw() = 0;
	virtual void exit() = 0;
	State();
	~State();
	int u_passthrough = false;
	int d_passthrough = false;
	//static int States;
};

