﻿#pragma once
#include "SFML/Graphics.hpp"
#include "Engine.h"
#include "Thor/Animations.hpp"
#include "ECS/ecs.h"
#include "Engine/AnimationData.h"
#include "States/Game/EnemyData.h"
#include "States/Game/Item.h"
#include "Pathfinding/stlastar.h"


/*
	Notes:
	Components are *supposed to be* logic-free data structures contained in entities,       <---- not anymore, they're logic filled now
	as well as tags for entities to be searched and iterated by.
	While the data in something such as an "exit" component honestly isnt needed much,
	it fills in type information for the entity, as an "entity" is just an int.
	Identical structures aren't typedefed as it caused issues earlier.

	All of the logic *should be* dumped into the iterable systems,                          <---- logic is now split :(
	abstracted away from the state itself as to make the state
	just a thin layer that doesn't need constant upkeep with code changes.

	Anything that needs to see inside GameState* needs to be defined in the companion file. <---- example of above detailed violation
	ODR prevents us seeing in GameState* from here. Luckily enough, passing it to a system
	is just find. Just makes sense for pathfinding components to generate their own grids
	instead of relying on a PFGridGenerator system.
*/

using namespace ecs;

enum class WallPos
{
	tlc, // Top left corner
	trc, // ..
	blc, // ..
	brc, // ..
	v2w, // Vertical 2 way
	h2w, // ..
	f,   // Full (empty)
	i,   // Isolated (all sides)
	u,   // 4way single directions
	d,   // ..
	l,   // .. 
	r    // ..

};

struct ComponentBase {
	ComponentBase() = default;
	ComponentBase(int id) noexcept : Id{id} {}
	int Id{0};
	int id() const {
		// this function is by default used
		// by the entity-component system to identify
		// elements, but this behaviour can be overridden
		// by specializing the ecs::element_id functor (see below)
		return Id;
	}
	// provide comparison operators because we use _ordered_ sets
	bool operator<(const ComponentBase& rhs) const {
		return Id < rhs.Id;
	}
	bool operator==(const ComponentBase& rhs) const {
		return Id == rhs.Id;
	}
};

struct Exit : public ComponentBase {
	using ComponentBase::ComponentBase;
	Exit(int id) : ComponentBase(id), active(false) {}
	Exit(int id, bool active, sf::Vector2i target) : ComponentBase(id), active(active), target(target) {}
	bool active;
	sf::Vector2i target;
};

enum class PFDirection {
	U,
	UR, //        U
	R,  //     UL | UR
	DR, //   L----+----R
	D,  //     DL | DR
	DL, //        D
	L,  
	UL
};


class PFNode
{
public:
	int x;	 // the (x,y) positions of the node
	int y;	
	
	PFNode() { x = y = 0; }
	PFNode( int px, int py ) { x=px; y=py; }

	float GoalDistanceEstimate( PFNode &nodeGoal );
	bool IsGoal( PFNode &nodeGoal );
	bool GetSuccessors( AStarSearch<PFNode> *astarsearch, PFNode *parent_node );
	float GetCost( PFNode &successor );
	bool IsSameState( PFNode &rhs );
	void PrintNodeInfo(); 
	int GetMap(int x, int y);
	static GameState* state;
};


// put one of these on anything with pathfinding t b h 
class Pathfinding {
public:
	Pathfinding(GameState* state) : state(state){
		Engine::Console()->info("Created pathfinding component -- is initialized");
	}
	Pathfinding(){
		Engine::Console()->info("Created pathfinding component without initializing state.\n\tThis will segfault without assigning later");
	}
	AStarSearch<PFNode> search;
	void calcPath(sf::Vector2i start, sf::Vector2i end);
	std::vector<PFNode*> path;
	sf::Vector2i lastTarget;
	GameState* state; // Useful for determining where walls are
};

struct Buffs {
	std::map<std::string, float> timers;
	std::map<std::string, int> modifiers;
	void add(std::string s, int modifier, float time) {
		if (modifiers.count(s) && timers.count(s)) {
			// normal ones
			if (s != "poison"){
				Engine::Console()->info("Modifying normal buff");
				modifiers[s] += modifier;
				timers[s] += time;
			}
			// exceptions
			if (s == "poison"){
				if (modifiers[s] < modifier){
					modifiers[s] = modifier;
				}
				Engine::Console()->info("Adding to poison timer... was {} now + {}", timers[s], (float)(std::floor(time*0.5)));
				timers[s] += (float)(std::floor(time/0.5));
			}
			Engine::Console()->info("Modifying previous buff [{}]...\n\t[{}] +{} for {} seconds\n\t[{}] is changed by +{} for {} seconds", s, s, modifiers[s]-modifier, timers[s]-time, s, modifier, time);
		} 
		else {
			Engine::Console()->info("Adding new buff...\n\t[{}] +{} for {} seconds", s, modifier, time);
			timers.emplace(s, time);
			modifiers.emplace(s, modifier);
		}
	}
	void remove(std::string s) {
		if (timers.count(s) && modifiers.count(s)) {
			timers.erase(s);
			modifiers.erase(s);
			Engine::Console()->info("Removed buff [{}].", s);
		}
		else {
			Engine::Console()->warn("Attempting to erase nonexistant buff [{}]", s);
		}
	}
	void clear() {
		modifiers.clear();
		timers.clear();
	}
	void update(float dt) {
		for (std::map<std::string, float>::iterator t = timers.begin(); t != timers.end();) {
			// BOOKMARK: buff timer < -10 => infinite duration
			const float infTimerCutoff = -10.0f;
			if (t->second > infTimerCutoff) {
				t->second -= dt; //
				if (t->second < 0.0f) {
					Engine::Console()->info("Time expiration on buff [{}]; removing.", t->first);
					if (t->first == "poison") {
						if (timers.count("poisonTick")){
							remove((t++)->first);
						}
						if (t->first == "poison"){
							remove((t++)->first);
						}
					}
					else {
						remove((t++)->first);
					}
				} else {
					t++;
				}
			} else {
				t++;
			}
		}
	}
	Buffs() {

	}
};

// ctor :: none yet, possibly ever.
struct Stats {
	int mhp;
	int chp;
	int ms;
	int mmp;
	int cmp;
	int str;
	int dex;
	int int_; // :(

	Buffs b;
	Stats() {
		mhp = 20; chp = 20; ms = 200; mmp = 10; cmp = 10; str = 5; dex = 5; int_ = 5;
	}
	Stats(Buffs bp) { Stats(); b = bp; }
	void updateBuffs(float dt) {
		b.update(dt);
	} 
	void addBuff(std::string stat, int modifier, float timer) {
		b.add(stat, modifier, timer);
	}	
	void addBuff(std::string stat, int modifier, bool infinite) {
		if (infinite) {
			b.add(stat, modifier, -100.0f);
		}
	}
	std::vector<std::string> getBuffsList() {
		std::vector<std::string> ret;
		//Engine::Console()->info("Getting buffs...");
		for (auto& e : b.modifiers) {
			ret.push_back(e.first);
			//Engine::Console()->info("\t{}",e.first);
		}
		return ret;
	}
	void removeBuff(std::string stat) {
		b.remove(stat);
	}
	void clearBuffs() {
		b.clear();
	}
	Stats get() {
		Stats s;
		#define getb(a) b.modifiers[a]
		s.mhp = mhp + str  + getb("mhp") + getb("str");
		s.chp = chp + str  + getb("chp") + getb("str");
		s.ms =  ms  + dex  * 10 + getb("ms") + getb("dex") * 10;
		s.mmp = mmp + int_ + getb("mmp") + getb("int_");
		s.cmp = cmp + int_ + getb("cmp") + getb("int_");
		s.str = str + getb("str");
		s.dex = dex + getb("dex");
		s.int_ = int_ + getb("int_");
		#undef getb
		return s;
	}
	bool isDead() {
		return get().chp > 0 ? false : true;
	}
	void applyDamage(int damage) {
		chp -= damage;
		if (get().chp < 0) chp = (mhp-get().mhp);
	}
	//json loading eventually? needed for sure...
};

struct Inventory {
	Inventory() {}
	std::vector<Weapon> weapons;
	Weapon* equippedWeapon;
};

// ctor :: (int id)
// ctor :: (int id, Stats s)
// ctor :: (int id, Stats s, Buffs b)
struct Player : public ComponentBase {
	using ComponentBase::ComponentBase;
	Player(int id) : ComponentBase(id) {}
	Player(int id, Stats s) : ComponentBase(id), s(s) {}
	Player(int id, Stats s, Inventory i) : ComponentBase(id), s(s), i(i) {}
	Inventory i;
	Stats s;
};

// ctor :: (int id)
// ctor :: (int id, Stats s)
// ctor :: (int id, Stats s, Buffs b)
struct Enemy : public ComponentBase {
	using ComponentBase::ComponentBase;
	// Not really sure if these are needed.
	Enemy(int id, EnemyClass c) : ComponentBase(id) { d_init(c); }
	Enemy(int id, EnemyClass c, Stats s) : ComponentBase(id), s(s) { d_init(c); }
	Enemy(int id, EnemyClass c, Stats s, Inventory i) : ComponentBase(id), s(s), i(i) { d_init(c); }
	Enemy(int id, EnemyData ed, Stats s, Inventory i, Pathfinding pf) : ComponentBase(id), ed(ed), s(s), i(i), pf(pf) {}
	void d_init(EnemyClass c) {
		ed.eclass = c;
		ed.behavior = AIState::Idle;
	}
	Inventory i;
	Stats s;
	EnemyData ed;
	Pathfinding pf;
	std::map<std::string, thor::Timer> timers;
	void addTimer(std::string key, float time){
		timers[key] = thor::Timer();
		timers[key].restart(sf::seconds(time));
	}
	bool checkTimer(std::string key){
		return timers[key].isExpired();
	}
	void removeTimer(std::string key){
		timers.erase(key);
	}
	bool timerExists(std::string key){
		return (timers.find(key) != timers.end());
	}
	void swing(GameState* state, float angle, sf::Vector2f origin){
		i.equippedWeapon->swing(state, angle, origin, false);
	}
};

// ctor :: (int id, int x, int y)
struct Position : public ComponentBase { 
	using ComponentBase::ComponentBase;
	Position(int id, int x, int y)
		: ComponentBase(id)
		, x(x)
		, y(y) {}
	int x;
	int y;
};

// ctor :: (int id, int x, int y)
struct Velocity : public ComponentBase {
	using ComponentBase::ComponentBase;
	Velocity(int id, int x, int y)
		: ComponentBase(id)
		, x(x)
		, y(y) {}
	int x;
	int y;
};


struct UIText {
	UIText(){}
	UIText(std::string font, int size = 12, std::string content = "", sf::Vector2f position = sf::Vector2f(0,0), sf::Color fcolor = sf::Color::White, sf::Color bcolor = sf::Color::Black){
		text.setFont((*Engine::Fonts)[font]);
		text.setString(content);
		text.setCharacterSize(size);
		text.setPosition(position);
		text.setFillColor(fcolor);
		text.setOutlineColor(bcolor);
	}
	void centerAt(sf::Vector2f center){
		sf::FloatRect bounding = text.getLocalBounds();
		sf::Vector2f nc;
		nc.x = center.x - (bounding.width  / 2);
		nc.y = center.y - (bounding.height / 2) - bounding.top;
		text.setPosition(nc);
	}
	sf::RectangleShape drawBounds(){
		sf::RectangleShape rs;
		sf::FloatRect bounding = text.getLocalBounds();
		rs.setSize(sf::Vector2f(bounding.width, bounding.height));
		rs.setPosition(bounding.left + text.getPosition().x, bounding.top + text.getPosition().y);
		rs.setOutlineColor(sf::Color::Red);
		rs.setFillColor(sf::Color::Transparent);
		rs.setOutlineThickness(2.0f);
		return rs;
	}
	sf::Text text;
};

struct Text : public ComponentBase {
	using ComponentBase::ComponentBase;
	Text(int id) : ComponentBase(id) {}
};

// ctor :: (int id, sf::Texture t, bool animated, int width, int height, int ox=0, int oy=0)
// ctor :: (int id, sf::Texture t, bool animated, int width, int height, AnimNames names, Anims anims, AnimTimes times, Times lengths, std::string InitialState, int ox = 0, int oy = 0)
// addFrame :: (std::string name, std::vector<std::pair<int,int>> frames, float time, std::vector<float> rtime)
struct Sprite : public ComponentBase {
	using ComponentBase::ComponentBase;
	Sprite(int id, sf::Texture* t, bool animated, int width, int height, int ox=0, int oy=0)
		: ComponentBase(id)
		, texture(t)
		, isAnimated(animated)
		, width(width)
		, height(height)
		, ox(ox)
		, oy(oy)
	{}
	Sprite(int id, sf::Texture* t, bool animated, int width, int height, AnimationData data, int ox = 0, int oy = 0)
		: ComponentBase(id)
		, texture(t)
		, isAnimated(animated)
		, width(width)
		, height(height)
		, data(data)
		, ox(ox)
		, oy(oy)
	{
		updateAnimationsFromData();
	}
	bool isAnimated;
	sf::Texture* texture;
	int ox; int oy;
	int width;
	int height;
	sf::Sprite sprite; // internal drawable for animation
	std::vector<thor::FrameAnimation> frames;
	thor::Animator<sf::Sprite, std::string> anim;
	AnimationData data;
	void updateAnimationsFromData() {
		for (uint16_t i = 0; i < data.anims.size(); ++i) {
			Frames f = data.anims[i];
			std::string animName = data.names[i];
			Times t = data.times[i];
			float l = data.lengths[i];
			addFrame(animName, f, l, t);
		}
		anim.playAnimation(data.defaultState, true);
	}
	void addFrame(std::string name, Frames frames, float time, Times rtime) {
		thor::FrameAnimation f;
		for (uint16_t i = 0; i < frames.size(); ++i) {
			sf::IntRect rect = sf::IntRect(frames[i].first*width, frames[i].second*height, width, height);
			f.addFrame(rtime[i], rect);
		}
		_addFrame(name, f, time);
	}
	void _addFrame(std::string n, thor::FrameAnimation f, float t) {
		frames.push_back(f);
		anim.addAnimation(n, frames.back(), sf::seconds(t));
	}
	// nice wrapper for not checking animations to cut them off
	void playAnimation(std::string name, bool loop){
		if (anim.isPlayingAnimation()){
			if (anim.getPlayingAnimation() != name){
				anim.playAnimation(name, loop);
			}
		}
		else {
			anim.playAnimation(name, loop);
		}
	}
};

struct Rectangle : public ComponentBase {
	using ComponentBase::ComponentBase;
	Rectangle(int id, sf::FloatRect rect, sf::Color color) : ComponentBase(id) 
	{
		drawable.setSize(sf::Vector2f(rect.width, rect.height));
		drawable.setPosition(sf::Vector2f(rect.left, rect.top));
		drawable.setFillColor(color);
	}
	sf::RectangleShape drawable;
};

// ctor :: (int id, float x, float y, float w, float h, bool _static) :: => single true
// ctor :: (int id, sf::FloatRect fr, bool _static) :: => single true
// ctor :: (int id, std::vector<sf::FloatRect> rs_, bool _static :: => single false
struct Collision : public ComponentBase {
	using ComponentBase::ComponentBase;
	// Singlerect float constructor
	Collision(int id, float x, float y, float w, float h, bool _static) :
		ComponentBase(id)
		, rs({sf::FloatRect(x,y,w,h)})
		, single(true)
		, _static(_static)
		{
			offsetFromPosition.push_back(sf::Vector2i(0,0));
		}
	// Singlerect rect constructor
	Collision(int id, sf::FloatRect fr, bool _static) :
		ComponentBase(id)
		, rs({fr})
		, single(true)
		, _static(_static)
		{
			offsetFromPosition.push_back(sf::Vector2i(0,0));
		}
	Collision(int id, std::vector<sf::FloatRect> rs_, bool _static) :
		ComponentBase(id)
		, rs(rs_)
		, single(false)
		, _static(_static)
		{}
	sf::Vector2f lvelocity = sf::Vector2f(0, 0);
	std::vector<sf::Vector2i> offsetFromPosition;
	bool _static;
	std::vector<sf::FloatRect> rs; // rigidbody with nonmoveable parts comprised of rects
	bool single;
};

/*struct VertexArrayDrawable : public ComponentBase, public sf::Drawable, public sf::Transformable {
	using ComponentBase::ComponentBase;
	VertexArrayDrawable(int id):
		ComponentBase(id)
		{}
	VertexArrayDrawable(int id, sf::VertexArray varray, sf::RenderStates rstates, sf::Vector2i size) :
		ComponentBase(id)
		, varray(varray)
		, rstates(rstates)
		{
		updateRStates();
		varray.setPrimitiveType(sf::Quads);
		varray.resize(size.x * size.y * 4);
	}
	void updateRStates() {
		rstates.transform *= getTransform();
		rstates.texture = &texture;
	}
	void draw() {
		Engine::Window->draw(this->varray, this->rstates);
	}
	int id;
	sf::VertexArray varray;
	sf::Texture texture;
	sf::RenderStates rstates;
};*/

