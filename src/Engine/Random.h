#pragma once
#include "pcg/pcg_random.hpp"
#include <random>
// the self contained dice themselves
enum class Distribution{
	Uniform, 
	Normal
};
class Random {
public:
	Random() : Random(0,0,10,0,Distribution::Uniform) {}
	Random(int seed) : Random(seed,0,10,0,Distribution::Uniform) {}
	Random(int seed, int lower, int upper) : Random(seed, lower, upper, 0, Distribution::Normal) {}
	Random(int seed, int lower, int upper, int addition) : Random(seed, lower, upper, addition, Distribution::Uniform) {}
	Random(int seed, int lower, int upper, Distribution dist) : Random(seed, lower, upper, 0, dist) {}
	Random(int seed, int lower, int upper, int addition, Distribution dist) {
		dType = dist;
		add   = addition;
		if (dist == Distribution::Uniform){
			uDist = std::uniform_int_distribution<int>(lower, upper);
		}
		if (dist == Distribution::Normal){
			nDist = std::normal_distribution<>(lower+upper/2, 2);
		}
		rng = pcg32(seed);
	}
	int roll() {
		if (dType == Distribution::Uniform) {
			return (uDist(rng)) + add;
		}
		if (dType == Distribution::Normal) {
			return (nDist(rng)) + add;
		}
	}
	float rollF(int decimals) {
		int mut = 10*decimals;
		if (dType == Distribution::Uniform) {
			std::uniform_int_distribution<int> _dDist(uDist.min() * mut, uDist.max() * mut);
			return (float)(_dDist(rng) + (add*mut)) / (float)mut;
		}
		if (dType == Distribution::Normal) {
			std::normal_distribution<> _dDist((nDist.min() * mut + nDist.max() * mut)/2, nDist.mean());
			return (float)(_dDist(rng) + (add*mut)) / (float)mut;
		}
	}
	Distribution 					   dType;
	std::uniform_int_distribution<int> uDist;
	std::normal_distribution<>         nDist;
	int 								 add;
	pcg32 								 rng;

};
// factory for giving dice to each instance
class RandomGenerator {
public:
	RandomGenerator(int seed) : _rng(seed), _seed(seed) {}
	RandomGenerator() {
		pcg_extras::seed_seq_from<std::random_device> seed;
		_rng  = pcg32(seed);
		_seed = _rng();
	}
	pcg32 _rng;
	int _seed;
	Random createRandom()
	{return Random();}
	Random createRandom(int seed) 
	{return Random(seed);}
	Random createRandom(int seed, int lower, int upper) 
	{return Random(seed, lower, upper);}
	Random createRandom(int seed, int lower, int upper, int addition) 
	{return Random(seed, lower, upper, addition);}
	Random createRandom(int seed, int lower, int upper, Distribution dist) 
	{return Random(seed, lower, upper, dist);}
	Random createRandom(int seed, int lower, int upper, int addition, Distribution dist) 
	{return Random(seed, lower, upper, addition, dist);}

	int roll(int lower, int upper) {
		return ((std::uniform_int_distribution<int>(lower, upper)) (_rng));
	}
};
