#include "Engine.h"
#include "States/GameState.h"
#include "State.h"

// Statics
sf::RenderWindow *Engine::Window = nullptr;
thor::ActionMap<Actions> *Engine::Input = nullptr;
thor::ResourceHolder<sf::Texture, std::string> *Engine::Textures = nullptr;
thor::ResourceHolder<sf::Font, std::string> *Engine::Fonts = nullptr;
thor::ResourceHolder<sf::Shader, std::string> *Engine::Shaders = nullptr;
std::vector<State*> *Engine::States = nullptr;
bool Engine::InputBlocker = false;
bool Engine::drawFreeze = false;
Engine* Engine::Instance = nullptr;
RandomGenerator* Engine::RNG = nullptr;

Engine::Engine(){
}

Engine::~Engine(){
}

void Engine::initWindow(bool fullscreen) {
	e_rng = RandomGenerator();
	sf::ContextSettings cs = sf::ContextSettings();
	cs.antialiasingLevel = 4U;
	if (!fullscreen) {
		e_window = new sf::RenderWindow(sf::VideoMode(1280, 720), "RL", sf::Style::Default, cs);
	}
	else {
		e_window = new sf::RenderWindow(sf::VideoMode::getFullscreenModes()[0], "RL", sf::Style::Fullscreen, cs);
	}
	Engine::Window = e_window;
	e_window->setKeyRepeatEnabled(false);
	e_window->setMouseCursorVisible(false);
}

void Engine::start() {

	initWindow(e_isFullscreen);
	initKeybinds();
	Engine::Window = e_window;
	Engine::Textures = &e_textures;
	Engine::Fonts = &e_fonts;
	Engine::Shaders = &e_shaders;
	Engine::States = &e_states;
	Engine::Instance = this;
	Engine::RNG = &e_rng;

	GameState* gs = new GameState();
	pushState(gs);


	Engine::Console()->set_level(spdlog::level::info);
	while (e_window->isOpen()) {
		// Check for a current state
		if (e_stateCount == 0 && !Engine::InputBlocker)
			e_actions.update(*e_window);
		if (e_actions.isActive(Actions::Quit)) {
			e_window->close();
		}
		if (e_stateCount > 0) {
			State* cstate = e_states.back();
			while (e_acc > e_target) {
				if (!Engine::InputBlocker)
					e_actions.update(*e_window);
				e_acc -= e_target;
				update(cstate, e_target.asSeconds());
				if (cstate->u_passthrough) {
					update(cstate - 1, e_target.asSeconds());
				}
			}
			if (!Engine::drawFreeze) {
				e_window->clear(sf::Color(127, 127, 127, 255));
				draw(cstate);
				if (cstate->d_passthrough) {
					draw(cstate - 1);
				}
				e_window->display();
			}
		}
		e_acc += e_clock.restart();
		log->debug("Extrapolated FPS: {}", 1.0f/e_acc.asSeconds());
	}
	exit();
}

void Engine::update(State *state, float dt) {
	if (e_actions.isActive(Actions::Fullscreen)) {
		e_window->close();
		e_isFullscreen = !e_isFullscreen;
		initWindow(e_isFullscreen);
	}
	state->update(dt);
}

void Engine::draw(State *state) {
	state->draw();
}

void Engine::exit() {
	e_window->close();
}

void Engine::pushState(State * state)
{
	e_stateCount++;
	e_states.push_back(state);
	e_states.back()->enter();
}

void Engine::popState(State * state)
{
	e_stateCount--;
	e_states.back()->exit();
	e_states.pop_back();
}

void Engine::initKeybinds() {
	e_actions[Actions::Quit] = actionClose;
	e_actions[Actions::Left] = actionLeft;
	e_actions[Actions::Right] = actionRight;
	e_actions[Actions::Up] = actionUp;
	e_actions[Actions::Down] = actionDown;
	e_actions[Actions::NMH] = actionNMH;
	e_actions[Actions::NMV] = actionNMV;
	e_actions[Actions::LClick] = actionLClick;
	e_actions[Actions::LClickHold] = actionLClickHold;
	e_actions[Actions::RClick] = actionRClick;
	e_actions[Actions::RClickHold] = actionRClickHold;
	e_actions[Actions::MMC] = actionMMC;
	e_actions[Actions::dbgNewMap] = actionDbgNewMap;
	e_actions[Actions::DBGF2] = actionDBGF2;
	e_actions[Actions::DBGF3] = actionDBGF2;
	e_actions[Actions::DBGF4] = actionDBGF2;
	e_actions[Actions::DBGF5] = actionDBGF2;
	e_actions[Actions::Fullscreen] = actionFullscreen;
	e_actions[Actions::Resize] = actionResize;
	Engine::Input = &e_actions;
}
