#pragma once
#include<vector>

typedef std::vector<std::vector<std::pair<int, int>>> Anims;
typedef std::vector<std::pair<int, int>> Frames;
typedef std::vector<float> Times;
typedef std::vector<std::vector<float>> AnimTimes;
typedef std::vector<std::string> AnimNames;

struct AnimationData {
	Anims anims;
	AnimNames names;
	AnimTimes times;
	Times lengths;
	std::string defaultState;
	void addAnimation(std::string name, Frames data, Times relLength, float totalLength) {
		names.push_back(name);
		anims.push_back(data);
		times.push_back(relLength);
		lengths.push_back(totalLength);
		defaultState = names[0];
	}
	void addAnimation(std::string name, Frames data, float totalLength) {
		Times reltimes;
		for (uint16_t i = 0; i < data.size(); ++i) {
			reltimes.push_back(1.0f);
		}
		addAnimation(name, data, reltimes, totalLength);
	}
	void addAnimations(AnimNames names, Anims anims, AnimTimes relTimes, Times lengths) {
		for (uint16_t i = 0; i < anims.size(); ++i) {
			addAnimation(names[i], anims[i], relTimes[i], lengths[i]);
		}
	}
	void addAnimations(AnimNames names, Anims anims, Times lengths) {
		AnimTimes relTimes;
		for (uint16_t i = 0; i < anims.size(); ++i) {
			Times t;
			for (uint16_t j = 0; j < anims[i].size(); ++j) {
				t.push_back(1.0f);
			}
			relTimes.push_back(t);
		}
		addAnimations(names, anims, relTimes, lengths);
	}
	void clear() {
		anims.clear();
		names.clear();
		times.clear();
		lengths.clear();
	}
};