#include "Components.h"
#include "States/GameState.h" //ODR workaround
#include <vector>
#include "Tilemap/Tile.h"
#include "Tilemap/GameMap.h"


// pathfinding garbage
// V V V V V V V V V V
float v2iDist(sf::Vector2i* a, sf::Vector2i* b){ return std::sqrt(std::pow(a->x-b->x, 2) + std::pow(a->y-b->y, 2)); }

void Pathfinding::calcPath(sf::Vector2i start, sf::Vector2i end){
	path.clear();
	GameMapTile* gmt = &state->gamemap.map[state->gamemapPos];

	const int width = gmt->data.w;
	const int height = gmt->data.h;

	PFNode startn(start.x, start.y);
	PFNode endn(end.x, end.y);

	search.SetStartAndGoalStates(startn, endn);
	unsigned int searchState;
	unsigned int searchSteps = 0;

	do {
		searchState = search.SearchStep();
		searchSteps++;

	} while(searchState == AStarSearch<PFNode>::SEARCH_STATE_SEARCHING);

	if( searchState == AStarSearch<PFNode>::SEARCH_STATE_SUCCEEDED ) {
		PFNode* node = search.GetSolutionStart();
		path.push_back(node);
		for (;;){
			node = search.GetSolutionNext();
			if (!node) break;
			path.push_back(node);
		}
	}
	else if( searchState == AStarSearch<PFNode>::SEARCH_STATE_FAILED ) 
	{
		
	}
}

GameState* PFNode::state = nullptr;

int PFNode::GetMap( int x, int y )
{
	if ( x < 0 ||
	     x >= state->gamemap.map[state->gamemapPos].data.w ||
		 y < 0 ||
		 y >= state->gamemap.map[state->gamemapPos].data.h )
	{
		return 9;	 
	}
	TileTerrain terrain = state->gamemap.map[state->gamemapPos].data.tiles[sf::Vector2i(x,y)].data.terrain;
	if (terrain == TileTerrain::Normal) {return 1;} else {return 9;}
}

bool PFNode::IsSameState( PFNode &rhs )
{
	// same state in a maze search is simply when (x,y) are the same
	if( (x == rhs.x) &&
		(y == rhs.y) )
	{
		return true;
	}
	else
	{
		return false;
	}
}


void PFNode::PrintNodeInfo()
{
	Engine::Console()->info("Pathfinding node: {}, {}", x, y);
}

// Here's the heuristic function that estimates the distance from a Node
// to the Goal. 

float PFNode::GoalDistanceEstimate( PFNode &nodeGoal )
{
	return fabsf(x - nodeGoal.x) + fabsf(y - nodeGoal.y);	
}

bool PFNode::IsGoal( PFNode &nodeGoal )
{

	if( (x == nodeGoal.x) &&
		(y == nodeGoal.y) )
	{
		return true;
	}

	return false;
}

// This generates the successors to the given Node. It uses a helper function called
// AddSuccessor to give the successors to the AStar class. The A* specific initialisation
// is done for each node internally, so here you just set the state information that
// is specific to the application
bool PFNode::GetSuccessors( AStarSearch<PFNode> *astarsearch, PFNode *parent_node )
{

	int parent_x = -1; 
	int parent_y = -1; 

	if( parent_node )
	{
		parent_x = parent_node->x;
		parent_y = parent_node->y;
	}
	

	PFNode NewNode;

	// push each possible move except allowing the search to go backwards

	if( (GetMap( x-1, y ) < 9) 
		&& !((parent_x == x-1) && (parent_y == y))
	  ) 
	{
		NewNode = PFNode( x-1, y );
		astarsearch->AddSuccessor( NewNode );
	}	

	if( (GetMap( x, y-1 ) < 9) 
		&& !((parent_x == x) && (parent_y == y-1))
	  ) 
	{
		NewNode = PFNode( x, y-1 );
		astarsearch->AddSuccessor( NewNode );
	}	

	if( (GetMap( x+1, y ) < 9)
		&& !((parent_x == x+1) && (parent_y == y))
	  ) 
	{
		NewNode = PFNode( x+1, y );
		astarsearch->AddSuccessor( NewNode );
	}	

		
	if( (GetMap( x, y+1 ) < 9) 
		&& !((parent_x == x) && (parent_y == y+1))
		)
	{
		NewNode = PFNode( x, y+1 );
		astarsearch->AddSuccessor( NewNode );
	}	

	return true;
}

float PFNode::GetCost( PFNode &successor )
{
	return (float) GetMap( x, y );
}