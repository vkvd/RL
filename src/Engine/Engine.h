#pragma once
#include "SFML/Graphics.hpp"
#include "Thor/Graphics.hpp"
#include "Thor/Resources.hpp"
#include "Thor/Input.hpp"
#include "Thor/Config.hpp"
#include "Thor/Math.hpp"
#include "iostream"
#include "spdlog/spdlog.h"
#include "Engine/Random.h"

#define FRAMERATE 200.0f

// resizing, nice to have it global
#define WINDOWSIZE Engine::Window->getSize()
// Helpers to load and unload content
// t: object
// k: key to register the resource to
// p: path of the resource
#define THORLOAD(t,p) (thor::Resources::fromFile<t>(p))
#define E_TEXLOAD(k,p) (e_textures.acquire(k,THORLOAD(sf::Texture, p)))
#define E_FNTLOAD(k,p) (e_fonts.acquire   (k,THORLOAD(sf::Font,    p)))
#define E_SHDLOAD(k,p) (e_shader.acquire  (k,THORLOAD(sf::Shader,  p)))
#define E_TEXULOAD(k)  (e_textures.release(k))
#define E_FNTULOAD(k)  (e_fonts.release   (k))
#define E_SHDULOAD(k)  (e_shaders.release (k))

// externally availible unsafes with no exception checking
// you should not be using these
#define U_TEXLOAD(k,p) (Engine::Textures->acquire(k,THORLOAD(sf::Texture, p)))
#define U_FNTLOAD(k,p) (Engine::Fonts->acquire   (k,THORLOAD(sf::Font,    p)))
#define U_SHDLOAD(k,p) (Engine::Shaders->acquire (k,THORLOAD(sf::Shader,  p)))
#define TEXULOAD(k)  (Engine::Textures.release(k))
#define FNTULOAD(k)  (Engine::Fonts.release   (k))
#define SHDULOAD(k)  (Engine::Shaders.release (k))

// make em safe (use these)
#define BGNLOAD try {
#define ENDLOAD ;} catch (thor::ResourceAccessException& e) { \
    Engine::Console()->info("Resource loading warning: \n  \"{}\"\n  There's probably no issue here - a texture could've been loaded twice.", e.what()); \
}
#define TEXLOAD(k,p) \
    BGNLOAD \
    U_TEXLOAD(k,p) \
    ENDLOAD

#define FNTLOAD(k,p) \
    BGNLOAD \
    U_FNTLOAD(k,p) \
    ENDLOAD

#define SHDLOAD(k,p) \
    BGNLOAD \
    U_SHDLOAD(k,p) \
    ENDLOAD


class State;

enum class Actions {
	Quit,
	Pause,
	Left,
	Right,
	Up,
	Down,
	NMH,
	NMV,
	Dash,
	LClick,
	LClickHold,
	RClick,
	RClickHold,
	MMC,
	dbgNewMap,
	DBGF2,
	DBGF3,
	DBGF4,
	DBGF5,
	Fullscreen,
	Resize
};

class Engine
{
public:
	Engine();
	~Engine();
	// Main window pointed at by everything
	// Wrapped by Engine::window
	sf::RenderWindow *e_window;
	static sf::RenderWindow *Window;
	static thor::ActionMap<Actions> *Input;
	static thor::ResourceHolder<sf::Texture, std::string> *Textures;
	static thor::ResourceHolder<sf::Font, std::string> *Fonts;
	static thor::ResourceHolder<sf::Shader, std::string> *Shaders;
	static std::vector<State*> *States;
	static bool InputBlocker;
	static bool drawFreeze; 
	static Engine* Instance;
	static RandomGenerator* RNG;
	// framestep fixes
	sf::Clock e_clock;
	sf::Time e_acc = sf::Time::Zero;
	sf::Time e_target = sf::seconds(1.0f / FRAMERATE); 
	std::shared_ptr<spdlog::logger> log = spdlog::stdout_color_mt("Console");
	static std::shared_ptr<spdlog::logger> Console() {
		return spdlog::get("Console");
	}

	RandomGenerator e_rng;

	// Fullscreen check
	bool e_isFullscreen = false;

	// Create window
	void initWindow(bool fullscreen);

	// Initialize engine
	void start();
	
	// Update state
	void update(State *state, float dt);
	
	// Draw state
	void draw(State *state);
	
	// Closing
	void exit();

	// Initialize keybinds
	void initKeybinds();

	// State changing for enter/exit
	void pushState(State* state);
	void popState(State* state);

	// State counter to check for nulls
	int e_stateCount = 0;

	/* Possible state stack: (top gets updated/drawn)
	// Pause
	// Game
	// Menu */
	std::vector<State*> e_states;
	
	// Resources
	thor::ResourceHolder<sf::Texture, std::string> e_textures;
	thor::ResourceHolder<sf::Font, std::string> e_fonts;
	thor::ResourceHolder<sf::Shader, std::string> e_shaders;
	
	// General actions
	thor::Action actionClose      = thor::Action(sf::Event::Closed);
	// Keybinds 
	thor::Action actionLeft       = thor::Action(sf::Keyboard::A, thor::Action::Hold);
	thor::Action actionRight      = thor::Action(sf::Keyboard::D, thor::Action::Hold);
	thor::Action actionUp         = thor::Action(sf::Keyboard::W, thor::Action::Hold);
	thor::Action actionDown       = thor::Action(sf::Keyboard::S, thor::Action::Hold);
	thor::Action actionNMV        = !(actionUp || actionDown);
	thor::Action actionNMH        = !(actionLeft || actionRight);
	thor::Action actionLClick     = thor::Action(sf::Mouse::Left, thor::Action::PressOnce);
	thor::Action actionLClickHold = thor::Action(sf::Mouse::Left, thor::Action::Hold);
	thor::Action actionRClick     = thor::Action(sf::Mouse::Right, thor::Action::PressOnce);
	thor::Action actionRClickHold = thor::Action(sf::Mouse::Right, thor::Action::Hold);
	thor::Action actionMMC        = thor::Action(sf::Mouse::Middle, thor::Action::PressOnce);
	// Debug actions
	thor::Action actionDBGF2      = thor::Action(sf::Keyboard::Num2, thor::Action::PressOnce);
	thor::Action actionDBGF3      = thor::Action(sf::Keyboard::Num3, thor::Action::PressOnce);
	thor::Action actionDBGF4      = thor::Action(sf::Keyboard::Num4, thor::Action::PressOnce);
	thor::Action actionDBGF5      = thor::Action(sf::Keyboard::Num5, thor::Action::PressOnce);
	
	
	thor::Action actionDbgNewMap  = thor::Action(sf::Keyboard::Num1, thor::Action::PressOnce);
	// 4 actions for fullscreen: l/rAlt hold, l/rEnter push
	thor::Action actionFS1        = thor::Action(sf::Keyboard::LAlt, thor::Action::Hold);
	thor::Action actionFS2        = thor::Action(sf::Keyboard::RAlt, thor::Action::Hold);
	thor::Action actionFS3        = thor::Action(sf::Keyboard::Return, thor::Action::PressOnce);
	thor::Action actionFullscreen = (actionFS1 || actionFS2) && actionFS3;
	thor::Action actionResize     = thor::Action(sf::Event::Resized);
	// Full map
	thor::ActionMap<Actions> e_actions;
};