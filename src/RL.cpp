#include "SFML/Graphics.hpp"
#include "Engine/Engine.h"
// VS open console
#pragma comment(linker, "/SUBSYSTEM:CONSOLE /ENTRY:mainCRTStartup")

int main()
{
	Engine engine;
	engine.start();
    return 0;
}

