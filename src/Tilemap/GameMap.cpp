#include "GameMap.h"
#include "json/json.hpp"
#include <iostream>
#include <fstream>
#include <random>
#include "Engine/Engine.h"
#include "States/GameState.h"
#include "States/Game/DoorDirections.h"
#include <algorithm>




GameMap::GameMap()
{
 
}

GameMap::GameMap(GameState* state, int w, int h)
{
    generate(state, w, h);
}


void GameMap::generate(GameState* state, int w, int h)
{
    Engine::Console()->info("--- BEGIN MAP GEN ---");
    /*

    * choose a random non-edge point
    * create a room with 2+ exits
    * add exits to a queue
    * add finished rooms to a list
    * create a room off each exit that can't link in the direction of finished rooms
    * don't go in to already made walls
    * ignore uncreatable rooms
    or something
    * pop it off the queue and add it to the list
    * repeat

    !! special case for edges
    !! do enemies in a map style list and random fill it over the map

    !! current code corrupts stack :really: bad :12/6
    ---- not anymore but it might later :12/15
    !! just doesn't work 1/4
    TOTALLY WORKS 1/15

    */
    // Connection Queue Entry
    // place: what room its from
    // direction: which direction it connects from

    // const for how many need to be filled or throw it out (all of it)
    const float fillPercentL = 0.80f;
    const float fillPercentR = 1.00f;

    map.clear();
    this->w=w;
    this->h=h;
    struct CQ {
        sf::Vector2i place;
        EntryDir direction;
        CQ(sf::Vector2i p, EntryDir d) : place(p), direction(d) {}
        CQ() {}
    };
    std::vector<CQ> openConnectionsQueue;
    std::vector<sf::Vector2i> finishedRooms;
    // Choose point that isn't on the edge
    sf::Vector2i randpt = sf::Vector2i(Engine::RNG->roll(1,w-2), Engine::RNG->roll(1,h-2)); //why -2? w=5 => x={0,[1,2,3],4};
    Engine::Console()->debug("\tStarting room at x:{} y:{}", randpt.x, randpt.y);
    // Create a room at randpt with more than 2 connections
    int roomindex, connections;
    do {
        openConnectionsQueue.clear();
        connections = 0;
        roomindex = Engine::RNG->roll(1, possibleTiles.size()-1); // no 0ing
        for (int _i = 0; _i < 4; _i++) {
            if (this->possibleTiles[roomindex].jumps[_i]) { 
                CQ cq;
                cq.direction = ToEntryDir(_i);
                switch (cq.direction) {
                case EntryDir::Up:
                    cq.place = (randpt + sf::Vector2i(0, -1));
                    Engine::Console()->debug("\t\tAdding to connection queue: Up");
                    break;
                case EntryDir::Left:
                    cq.place = (randpt + sf::Vector2i(-1, 0));
                    Engine::Console()->debug("\t\tAdding to connection queue: Left");
                    break;
                case EntryDir::Right:
                    cq.place = (randpt + sf::Vector2i( 1, 0));
                    Engine::Console()->debug("\t\tAdding to connection queue: Right");
                    break;
                case EntryDir::Down:
                    cq.place = (randpt + sf::Vector2i( 0, 1));
                    Engine::Console()->debug("\t\tAdding to connection queue: Down");
                    break;
                }
                openConnectionsQueue.push_back(cq);
                connections++;
            }
        }
    } while (connections < 2);
    GameMapTile gmtile(this->possibleTiles,roomindex);
    map.insert({ sf::Vector2i(randpt),gmtile });
    spawn = randpt;
    finishedRooms.push_back(randpt);
    Engine::Console()->debug("\tAdded starting room: x:{} y:{} index:{}", randpt.x, randpt.y, roomindex);

    bool isGenerating = true;
    while (isGenerating) {
        int maxbound = openConnectionsQueue.size();
        for (int _i = 0; _i < maxbound; _i++) {
            Engine::Console()->debug("\tOpen connections: {}", openConnectionsQueue.size());
            // If the open connection is done, drop it
            // Should honestly never happen, just a sanity check
            // Erase/remove with reference capture lambda
            auto ifOpenAlreadyFinished = [&](sf::Vector2i FR){ return FR == openConnectionsQueue[_i].place; };
            finishedRooms.erase(std::remove_if(finishedRooms.begin(), finishedRooms.end(), ifOpenAlreadyFinished), finishedRooms.end());
            // Get directions that wouldn't link into a finished room
            // Add it, pop connection, finish the room
            CQ cq = openConnectionsQueue[_i];

            if (cq.place.x < 0 || cq.place.x > (w-1) || cq.place.y < 0 || cq.place.y > (h-1)) {
                openConnectionsQueue.erase(openConnectionsQueue.begin() + _i);
                maxbound--;
                _i--;
            }

            int roomindex;
            std::vector<sf::Vector2i> ntiles;
            bool connectionCheck;
            bool success = false; do {

                // Print game map each step, great for debug

                // Engine::Console()->debug("gamemap: w:{} h:{}",w, h);
                // for (int T1 = 0; T1 < h; ++T1) {
                //     std::cout << "\t";
                //     for (int T2 = 0; T2 < w; T2++) {
                //         int id = map[sf::Vector2i(T2, T1)].mapID;
                //         id > 9 ? std::cout << id << " " : std::cout << id << "  ";
                //     }
                //     std::cout << "\n";
                // }       

                roomindex = Engine::RNG->roll(1, possibleTiles.size()-1);
                Engine::Console()->debug("\tTrying room x:{} y:{} with id:{}", cq.place.x, cq.place.y, roomindex);
                // The directions it connected from
                std::vector<ExitDir> exitdirs;
                ntiles = getNeighboringTiles(cq.place);
                Engine::Console()->debug("\t\tNumber of neighboring tiles: {}", ntiles.size());
                for (auto ntile : ntiles) {
                    // room is above
                    Engine::Console()->debug("\t\t\tTesting for exitdirs (4 total)");
                    if (ntile != sf::Vector2i(-1,-1)){
                        if (ntile.y < cq.place.y) { //ntile above potentialCQ
                            if (state->gamemap.possibleTiles[state->gamemap.map[ntile].mapID].jumps[(int)EntryDir::Down]) {
                                exitdirs.push_back(ExitDir::Down);
                                Engine::Console()->debug("\t\t\t\tRoom needs exit direction Down");
                            }
                        }
                        // room is on the left
                        if (ntile.x < cq.place.x) {
                            if (state->gamemap.possibleTiles[state->gamemap.map[ntile].mapID].jumps[(int)EntryDir::Right]) {
                                exitdirs.push_back(ExitDir::Right);
                                Engine::Console()->debug("\t\t\t\tRoom needs exit direction Right");
                            }
                        }
                        // room is on the right
                        if (ntile.x > cq.place.x) {
                            if (state->gamemap.possibleTiles[state->gamemap.map[ntile].mapID].jumps[(int)EntryDir::Left]) {
                                exitdirs.push_back(ExitDir::Left);
                                Engine::Console()->debug("\t\t\t\tRoom needs exit direction Left");
                            }
                        }
                        // room is below
                        if (ntile.y > cq.place.y) {
                            if (state->gamemap.possibleTiles[state->gamemap.map[ntile].mapID].jumps[(int)EntryDir::Up]) {
                                exitdirs.push_back(ExitDir::Up);
                                Engine::Console()->debug("\t\t\t\tRoom needs exit direction Up");
                            }
                        }
                    }
                }
                connectionCheck = true;
                for (auto edir : exitdirs) {
                    if (!(possibleTiles[roomindex].jumps[(int)ExitToEntry(edir)])) {
                        connectionCheck = false;
                    }
                }
                for (int i = 0; i < 4; ++i) {
                    if (possibleTiles[roomindex].jumps[i]){
                        switch (i) {
                        case 0:
                        if (!possibleTiles[state->gamemap.map[cq.place+sf::Vector2i(0,-1)].mapID].jumps[3] &&
                            state->gamemap.map[cq.place+sf::Vector2i(0,-1)].mapID != 0)
                            {
                            connectionCheck = false;
                        }   
                        break;
                        case 1:
                        if (!possibleTiles[state->gamemap.map[cq.place+sf::Vector2i(-1,0)].mapID].jumps[2] &&
                            state->gamemap.map[cq.place+sf::Vector2i(-1,0)].mapID != 0){
                            connectionCheck = false;
                        }   
                        break;
                        case 2:
                        if (!possibleTiles[state->gamemap.map[cq.place+sf::Vector2i(1,0)].mapID].jumps[1] &&
                            state->gamemap.map[cq.place+sf::Vector2i(1,0)].mapID != 0){
                            connectionCheck = false;
                        }   
                        break;
                        case 3:
                        if (!possibleTiles[state->gamemap.map[cq.place+sf::Vector2i(0,1)].mapID].jumps[0] &&
                            state->gamemap.map[cq.place+sf::Vector2i(0,1)].mapID != 0){
                            connectionCheck = false;
                        }   
                        break;
                        default: break;
                        }
                    }
                }

                Engine::Console()->debug("\t\tTesting if the room connects: {}", connectionCheck);

                // room is not on edge
                if (cq.place.x != 0 && cq.place.y != 0 && cq.place.x != (w - 1) && cq.place.y != (h - 1) && exitdirs.size() > 0) {
                    Engine::Console()->debug("\t\tRoom not on edge");
                    if (connectionCheck) {
                        Engine::Console()->debug("\t\t\tRoom fulfills connections");
                        Engine::Console()->debug("\t\t\tAttempting to find matching room......");
                        success = true;
                        std::vector<bool> blocked;
                        blocked.resize(4);

                        if (success){   
                            GameMapTile gmtile(this->possibleTiles, roomindex);
                            map[cq.place]=gmtile;
                            Engine::Console()->debug("\t\t\t\tAdding room {} {} {}", cq.place.x, cq.place.y, roomindex);
                            //hackish erase remove... could cause bugs
                            Engine::Console()->debug("\t\t\t\tRemoving OCQ");
                            openConnectionsQueue.erase(openConnectionsQueue.begin() + _i);
                            finishedRooms.push_back(cq.place);
                            maxbound--;
                            _i--;
                            // add connections to queue
                            sf::Vector2i offset = sf::Vector2i(0, 0);
                            Engine::Console()->debug("\t\t\t\tAttempting to add doors to OCQ");
                            for (int i = 0; i < 4; i++){
                                if (possibleTiles[roomindex].jumps[i]){
                                    if (i==(int)EntryDir::Left){
                                        Engine::Console()->debug("\t\t\t\t\ttesting left");
                                        offset = sf::Vector2i(-1, 0);
                                        if (!(std::find(finishedRooms.begin(), finishedRooms.end(), cq.place+offset) != finishedRooms.end())){
                                            Engine::Console()->debug("\t\t\t\t\tadded left");
                                            openConnectionsQueue.push_back(CQ((cq.place + offset), ToEntryDir(i)));
                                        }                                       
                                    }
                                    if (i==(int)EntryDir::Right){
                                        Engine::Console()->debug("\t\t\t\t\ttesting right");
                                        offset = sf::Vector2i(1, 0);
                                        if (!(std::find(finishedRooms.begin(), finishedRooms.end(), cq.place+offset) != finishedRooms.end())){
                                            Engine::Console()->debug("\t\t\t\t\tadded right");
                                            openConnectionsQueue.push_back(CQ(cq.place + offset, ToEntryDir(i)));
                                        }                                       
                                    }
                                    if (i==(int)EntryDir::Up){
                                        Engine::Console()->debug("\t\t\t\t\ttesting Up");
                                        offset = sf::Vector2i(0, -1);
                                        if (!(std::find(finishedRooms.begin(), finishedRooms.end(), cq.place+offset) != finishedRooms.end())){
                                            Engine::Console()->debug("\t\t\t\t\tadded up");
                                            openConnectionsQueue.push_back(CQ(cq.place + offset, ToEntryDir(i)));
                                        }                                       
                                    }
                                    if (i==(int)EntryDir::Down){
                                        Engine::Console()->debug("\t\t\t\t\ttesting down");
                                        offset = sf::Vector2i(0, 1);
                                        if (!(std::find(finishedRooms.begin(), finishedRooms.end(), cq.place+offset) != finishedRooms.end())){
                                            Engine::Console()->debug("\t\t\t\t\tadded down");
                                            openConnectionsQueue.push_back(CQ(cq.place + offset, ToEntryDir(i)));
                                        }                                       
                                    }
                                }
                            }
                        }
                    }
                }
                // room is on edge
                else {
                    Engine::Console()->debug("\t\tRoom is on edge");
                    // Conditions: no past outer edge
                    // Entry direction
                    // 0
                    //1 2
                    // 3
                    if ( exitdirs.size() > 0 ){
                        bool n2 = true;
                        for (int i = 0; i < 4; i++) {
                            sf::Vector2i offset = sf::Vector2i(0, 0);
                            switch (i) {
                            case 0: offset = sf::Vector2i( 0,-1); break;
                            case 1: offset = sf::Vector2i(-1, 0); break;
                            case 2: offset = sf::Vector2i( 1, 0); break;
                            case 3: offset = sf::Vector2i( 0, 1); break;
                            }
                            if (possibleTiles[roomindex].jumps[i]) {
                                if (
                                    (cq.place + offset).y < 0   ||
                                    (cq.place + offset).y > h-1 ||
                                    (cq.place + offset).x < 0   ||
                                    (cq.place + offset).x > w-1 ){
                                    Engine::Console()->debug("\t\t\tFailed first test");
                                    n2 = false;
                                }
                                if (!state->gamemap.possibleTiles[state->gamemap.map[(cq.place+offset)].mapID].jumps[(int)ToExitDir(i)] &&
                                    std::find(finishedRooms.begin(), finishedRooms.end(), (cq.place+offset)) == finishedRooms.end()){
                                    Engine::Console()->debug("\t\t\tFailed second test");
                                    n2=false;
                                }
                            }
                        }
                        if (connectionCheck && n2) {
                            success = true;
                            GameMapTile gmtile(this->possibleTiles, roomindex);
                            map[cq.place]=gmtile;
                            finishedRooms.push_back(cq.place);
                            Engine::Console()->debug("\t\t\t\tAdding room {} {} {}", cq.place.x, cq.place.y, roomindex);
                            openConnectionsQueue.erase(openConnectionsQueue.begin() + _i);
                            maxbound--;
                            _i--;
                            sf::Vector2i offset = sf::Vector2i(0, 0);
                            for (int i = 0; i < 4; i++){
                                if (possibleTiles[roomindex].jumps[i]){
                                    if (i==(int)EntryDir::Left){
                                        Engine::Console()->debug("\t\t\t\t\ttesting left");
                                        offset = sf::Vector2i(-1, 0);
                                        if (!(std::find(finishedRooms.begin(), finishedRooms.end(), cq.place+offset) != finishedRooms.end())){
                                            Engine::Console()->debug("\t\t\t\t\tadded left");
                                            openConnectionsQueue.push_back(CQ((cq.place + offset), ToEntryDir(i)));
                                        }                                       
                                    }
                                    if (i==(int)EntryDir::Right){
                                        Engine::Console()->debug("\t\t\t\t\ttesting right");
                                        offset = sf::Vector2i(1, 0);
                                        if (!(std::find(finishedRooms.begin(), finishedRooms.end(), cq.place+offset) != finishedRooms.end())){
                                            Engine::Console()->debug("\t\t\t\t\tadded right");
                                            openConnectionsQueue.push_back(CQ(cq.place + offset, ToEntryDir(i)));
                                        }                                       
                                    }
                                    if (i==(int)EntryDir::Up){
                                        Engine::Console()->debug("\t\t\t\t\ttesting Up");
                                        offset = sf::Vector2i(0, -1);
                                        if (!(std::find(finishedRooms.begin(), finishedRooms.end(), cq.place+offset) != finishedRooms.end())){
                                            Engine::Console()->debug("\t\t\t\t\tadded up");
                                            openConnectionsQueue.push_back(CQ(cq.place + offset, ToEntryDir(i)));
                                        }                                       
                                    }
                                    if (i==(int)EntryDir::Down){
                                        Engine::Console()->debug("\t\t\t\t\ttesting down");
                                        offset = sf::Vector2i(0, 1);
                                        if (!(std::find(finishedRooms.begin(), finishedRooms.end(), cq.place+offset) != finishedRooms.end())){
                                            Engine::Console()->debug("\t\t\t\t\tadded down");
                                            openConnectionsQueue.push_back(CQ(cq.place + offset, ToEntryDir(i)));
                                        }                                       
                                    }
                                }
                            }
                        }
                    }
                }
                if (openConnectionsQueue.size() == 0){
                    isGenerating = false;
                }
                if (exitdirs.size() == 0){
                    Engine::Console()->debug("\t\tNo exits. Marking as finished.");
                    openConnectionsQueue.erase(openConnectionsQueue.begin() + _i);
                    maxbound--;
                    _i--;
                    success = true;
                    finishedRooms.push_back(cq.place);
                }
            } while (!success);
        }
        if (openConnectionsQueue.size() == 0){
            isGenerating = false;
        }
        Engine::Console()->debug("\tOCQ SIZE: {}", openConnectionsQueue.size());
    }
    int nFull = 0;
    for (int i = 0; i < h; ++i) {
        std::cout << "\t";
        for (int j = 0; j < w; j++) {
            int id = map[sf::Vector2i(j, i)].mapID;
            id > 9 ? std::cout << id << " " : std::cout << id << "  ";
            if(id != 0) nFull++;
        }
        std::cout << "\n";
    }
    bool single_end_print = true;
    Engine::Console()->info("Percent of rooms filled: {}% ... needs to be [{}% < x < {}%]", 100*(float)nFull/(float(w*h)), fillPercentL*100, fillPercentR*100);
    if (((float)nFull/(float)(w*h) > fillPercentR) || ((float)nFull/(float)(w*h) < fillPercentL)) {
        single_end_print = false;
        Engine::Console()->info("% outside of threshold, trying again.....");
        generate(state, w, h);
    }
    if (single_end_print) Engine::Console()->info("--- END MAP GEN ---");
}

void GameMap::setSpawn(GameState * state)
{
    state->gamemapPos = spawn;
}

void GameMap::loadJSON(std::string filename)
{
    Engine::Console()->info("--- BEGIN LOADING ROOM INFO ---");
    //load map
    using json = nlohmann::json;
    json data;
    std::ifstream s;
    s.open("content/rooms/rooms.json", std::ios::in);
    data << s;
    
    // Iterate over the tile list in the map, load each one 
    std::vector<std::string> nlsprites;
    for (auto& e : data["spritelist"]["Null"]) {
        std::string s = e.get<std::string>();
        TEXLOAD(s, "content/tiles/" + s + ".png");
        nlsprites.push_back(s);
    }
    // Add to randomizer
    spritelist[TileTerrain::Null] = nlsprites;

    std::vector<std::string> nrsprites;
    for (auto& e : data["spritelist"]["Normal"]) {
        std::string s = e.get<std::string>();
        TEXLOAD(s, "content/tiles/" + s + ".png");
        nrsprites.push_back(s);
    }
    spritelist[TileTerrain::Normal] = nrsprites;
    
    std::vector<std::string> wsprites;
    for (auto& e : data["spritelist"]["Wall"]) {
        std::string s = e.get<std::string>();
        TEXLOAD(s, "content/tiles/" + s + ".png");
        wsprites.push_back(s);
    }
    spritelist[TileTerrain::Wall] = wsprites;

    std::vector<std::string> dsprites;
    for (auto& e : data["spritelist"]["Door"]) {
        std::string s = e.get<std::string>();
        TEXLOAD(s, "content/tiles/" + s + ".png");
        dsprites.push_back(s);
    }
    spritelist[TileTerrain::Door] = dsprites;

    // Copy over each room data into map
    int roomcount = data["roomcount"];
    possibleTiles.clear();
    for (int i = 0; i < roomcount; i++){
        auto& e = data["rooms"][std::to_string(i)];
        MapData m;
        m.w = e["w"];
        m.h = e["h"];
        m.tw = data["th"];
        m.th = data["tw"];
        int x = 0;
        int y = 0;
        for (auto d : e["data"]) {
            Tile t(x, y);
            t.data.terrain = (TileTerrain)(int)d;
            m.tiles.insert({ sf::Vector2i(x,y), t });
            x++; 
            if (x > m.w-1) { x = 0; y++; }
        }
        for (int i = 0; i < 4; i++) m.jumps[i] = false;
        
        for (auto j : e["jumps"]) {
            m.jumps[j.get<int>()] = true;
        }
        Engine::Console()->debug("Tiles loaded so far: {}; Tiles loaded total: {}", possibleTiles.size(),i);
        Engine::Console()->debug("Tiledata:");
        for (int yy = 0; yy < m.h; yy++){
            std::cout << "\t\t";
            for (int xx = 0; xx < m.w; xx++){
                std::cout << " " << (int)e["data"][xx+yy*(int)e["w"]];
            }
            std::cout << "\n";
        }
        possibleTiles.push_back(m);
    }
    Engine::Console()->info("--- END LOADING ROOM INFO ---");
}

std::vector<sf::Vector2i> GameMap::getNeighboringTiles(sf::Vector2i loc)
{
    std::vector<sf::Vector2i> nbrs;
    Engine::Console()->debug("Getting neighboring tiles... w:{} h:{}", w, h);
    if (loc.y - 1 > 0) {
        nbrs.push_back(sf::Vector2i(loc.x, loc.y-1));
    }
    if (loc.y + 1 < h) {
        nbrs.push_back(sf::Vector2i(loc.x, loc.y+1));
    }
    if (loc.x - 1 > 0) {
        nbrs.push_back(sf::Vector2i(loc.x-1, loc.y));
    }
    if (loc.x + 1 < w) {
        nbrs.push_back(sf::Vector2i(loc.x+1, loc.y));
    }
    return nbrs;
}
