#pragma once
#include <math.h>
#include <vector>
#include <map>
#include "SFML/Graphics.hpp"
class GameState;

enum class TileTerrain {
	Null,
	Normal,
	Wall,
	Door,
	Hole,
	Water,
};

struct TileData {
	TileTerrain terrain;
	// anything else later
};

class Tile {
public:
	Tile() {}
	Tile(int x, int y) : x(x), y(y) {}
	~Tile() {}
	int x; int y;
	TileData data;
	int distance(Tile t){
		int ox = abs(t.x - x);
		int oy = abs(t.y - y);
		if (ox > oy) {
			return ox;
		}
		else {
			return oy;
		}
	}
};

// Operators

bool operator==(Tile a, Tile b);
bool operator!=(Tile a, Tile b);
Tile operator+(Tile a, Tile b);
Tile operator-(Tile a, Tile b);
Tile operator*(Tile a, int k);

struct TileTextures {
	std::vector<sf::Texture*> t;
	TileTextures(){}
	TileTextures(int len, sf::Texture* _t[]) {
		for (int i = 0; i < len; ++i) {
			t.push_back(_t[i]);
		}
	}
	TileTextures(std::vector<sf::Texture*> _t) : t(_t) {}
	sf::Texture* get(int x) {
		return (t[x]);
	}
	sf::Texture* get() {
		int r = rand() % t.size();
		return (t[r]);
	}
};

class TileMap {
public:
	TileMap(int w, int h, int tw, int th);
	TileMap();
	~TileMap();
	typedef std::map<sf::Vector2i, Tile> map_t;
	// Map width
	int w;
	// Map height
	int h;
	// Tile width
	int tw;
	// Tile height
	int th;
	// Main map data;  -> Tile 
	map_t tiles;
	// Terrain type -> Sprite array
	std::map<TileTerrain, TileTextures> textures;
	// Add an array of sprites for a tile type. (terrain, vector<texture*>)
	void addSprites(TileTerrain _tr, std::vector<sf::Texture*> _t);
	// Add an array of sprites for a tile type. (terrain, length of array, array of texture*)
	void addSprites(TileTerrain _tr, int _len, sf::Texture* _t[]);
	// Reset the map to normals
	void clearMap();
	// Get Tile reference at coordinate
	Tile& getTile(int x, int y);
	Tile& getTile(int a[2]) { return getTile(a[0], a[1]); }
	// Wrapper for getTile
	Tile& operator[](int a[2]);
	// Get nearby tiles
	std::vector<Tile*> get_neighbors(Tile t);
	std::vector<Tile*> get_4way(Tile t);
	std::vector<Tile*> get_diagonals(Tile t);
	// Mouse to tile
	Tile& coordsToTile(sf::Vector2f c);
	// Get
	std::vector<sf::Vector2f> getTileBounds(Tile t);
	// Create entities from the mapdata
	void createPrefabs(GameState* state, int tw, int th);
	// Find suitable player spawn
	sf::Vector2i findSafeSpawn(GameState* state);
};