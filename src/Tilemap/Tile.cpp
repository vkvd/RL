#include "Tile.h"
#include <iostream>
#include "Engine/Components.h"
#include "States/GameState.h"
#include "States/Game/EntityPrefabs.h"
#include "Engine/AnimationData.h"

// Tile Operators
bool operator==(Tile a, Tile b) {
	return a.x == b.x && a.y == b.y;
}
bool operator!=(Tile a, Tile b) {
	return !(a == b);
}
Tile operator+(Tile a, Tile b) {
	return Tile(a.x + b.x, a.y + b.y);
}
Tile operator-(Tile a, Tile b) {
	return Tile(a.x - b.x, a.y - b.y);
}
Tile operator*(Tile a, int k) {
	return Tile(a.x * k, a.y * k);
}

// TileMap

TileMap::TileMap(int w, int h, int tw, int th) : w(w),h(h),tw(tw),th(th)
{
	clearMap();
}
TileMap::TileMap() : w(5), h(5), tw(16), th(16)
{
	clearMap();
}
TileMap::~TileMap()
{
}

void TileMap::addSprites(TileTerrain _tr, std::vector<sf::Texture*> _t)
{
	textures[_tr] = TileTextures(_t);
}
void TileMap::addSprites(TileTerrain _tr, int _len, sf::Texture* _t[])
{
	textures[_tr] = TileTextures(_len, _t);
}

void TileMap::clearMap()
{
	tiles.clear();
	for (int y = 0; y < h; ++y) {
		for (int x = 0; x < w; x++) {
			sf::Vector2i loc = sf::Vector2i(x, y);
			Tile nh = Tile(loc.x, loc.y);
			nh.data.terrain = TileTerrain::Normal;
			tiles.insert({loc, nh});
		}
	}
}

Tile& TileMap::getTile(int x, int y)
{
	sf::Vector2i loc = sf::Vector2i(x, y);
	return tiles.find(loc)->second;
}
Tile& TileMap::operator[](int a[2])
{
	return getTile(a[0], a[1]);
}

std::vector<Tile*> TileMap::get_neighbors(Tile t)
{
	int tx, ty;
	std::vector<Tile*> r;
	for (ty = t.y + 1; ty >= t.y - 1; --ty) {
		for (tx = t.x + 1; ty >= t.x - 1; --tx){
			if (!(tx == t.x && ty == t.y) && !(tx < 0) && !(tx > w) && !(ty < 0) && !(ty > h)){
				r.push_back(&getTile(tx, ty));
			}
		}
	}
	return r;
}

std::vector<Tile*> TileMap::get_4way(Tile t)
{
	int tx, ty;
	std::vector<Tile*> r;
	for (ty = t.y + 1; ty >= t.y - 1; --ty) {
		for (tx = t.x + 1; ty >= t.x - 1; --tx) {
			if (!(tx == t.x && ty == t.y) && (tx==t.x || ty==t.y) && !(tx < 0) && !(tx > w) && !(ty < 0) && !(ty > h)) {
				r.push_back(&getTile(tx, ty));
			}
		}
	}
	return r;
}

std::vector<Tile*> TileMap::get_diagonals(Tile t)
{
	int tx, ty;
	std::vector<Tile*> r;
	for (ty = t.y + 1; ty >= t.y - 1; --ty) {
		for (tx = t.x + 1; ty >= t.x - 1; --tx) {
			if (!(tx == t.x && ty == t.y) && !(tx == t.x || ty == t.y) && !(tx < 0) && !(tx > w) && !(ty < 0) && !(ty > h)) {
				r.push_back(&getTile(tx, ty));
			}
		}
	}
	return r;
}

Tile& TileMap::coordsToTile(sf::Vector2f c)
{
	int tx; int ty;
	tx = (int)std::floor(c.x / tw);
	ty = (int)std::floor(c.y / th);
	return getTile(tx, ty);
}

std::vector<sf::Vector2f> TileMap::getTileBounds(Tile t)
{
	std::vector<sf::Vector2f> points;
	points.push_back(sf::Vector2f((float)t.x * tw,      (float)t.y * th));
	points.push_back(sf::Vector2f((float)t.x * tw + tw, (float)t.y * th));
	points.push_back(sf::Vector2f((float)t.x * tw + tw, (float)t.y * th + th));
	points.push_back(sf::Vector2f((float)t.x * tw,      (float)t.y * th + th));
	return points;
}

void TileMap::createPrefabs(GameState* state, int tw, int th) {
	int c_door = 0;
	for (map_t::iterator it = tiles.begin(); it != tiles.end(); it++) {
		//tileposition 
		//auto p = it->first;
		Tile& t = getTile(it->first.x, it->first.y);
		sf::Vector2i position = sf::Vector2i(t.x*tw, t.y*th);
		sf::Texture tex = *textures[t.data.terrain].get();
		// Get corners for drawing
		if (t.data.terrain == TileTerrain::Normal || t.data.terrain == TileTerrain::Null) {
			Prefabs::sprite(state, t.x*tw, t.y*th, textures[t.data.terrain].get(), false, tw, th, 0, 0);
		}
		if (t.data.terrain == TileTerrain::Wall) {
			Prefabs::wall(state, t.x*tw, t.y*th, tw, th, WallPos::i, textures[t.data.terrain].get());
		}
		if (t.data.terrain == TileTerrain::Door) {
			// Skip nonexistant doors and advance the placement index
			while (!state->gamemap.map[state->gamemapPos].jumps[c_door]) {
				c_door++;
			}
			sf::Vector2i target;
			// Sanity check for door's existance
			if (state->gamemap.map[state->gamemapPos].jumps[c_door]) {
				// Place it by index
				switch (c_door) {
				case 0:
					target = state->gamemapPos - sf::Vector2i(0, 1);
					break;
				case 1:
					target = state->gamemapPos - sf::Vector2i(1, 0);
					break;
				case 2:
					target = state->gamemapPos + sf::Vector2i(1, 0);
					break;
				case 3:
					target = state->gamemapPos + sf::Vector2i(0, 1);
					break;
				case 4: // Error case for now - rooms have 4 exits max
					target = sf::Vector2i(0, 0);
					Engine::Console()->critical("Room shouldn't have 0 exits. Something went wrong...?");
					break;
				}
			}
			// Place the exit with the door texture for that direction
			Prefabs::exit(state, false, t.x*tw, t.y*th, textures[t.data.terrain].get(c_door), tw, th, target);
			c_door++;
		}
	}
}
// lol
sf::Vector2i TileMap::findSafeSpawn(GameState* state) {
	return sf::Vector2i(90, 90);
}