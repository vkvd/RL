#pragma once
#include <map>
#include "Tile.h"


// Stripped TileMap used in recreating from json
struct MapData {
	typedef std::map<sf::Vector2i, Tile> map_t;
	int w;
	int h;
	int tw;
	int th;
	map_t tiles;
	bool jumps[4];
};

struct GameMapTile {
	int mapID; // Which room in rooms.json to use
	bool cleared; // Has the player cleared the enemies
	bool seen; // Has the player been adjacent (for minimap)
	bool jumps[4]; // Which doors does it have ULRD
	bool isExit; // Is it the floor exit
	MapData data; // Stripped tilemap
	sf::Vector2i targets[4]; // Where do the jumps go
	GameMapTile(std::vector<MapData> pt, int id) : mapID(id), cleared(false), seen(false) {
		data = pt[id];
		for (int x = 0; x < 4; x++) jumps[x] = data.jumps[x];
		// Targets is set later per door
	}
	GameMapTile() : mapID(0), cleared(false), seen(false) {
	}
	//TODO: items lmao
	std::map<sf::Vector2f, int> itemsOnFloor;
};

class GameMap {
public:
	std::map<sf::Vector2i, GameMapTile> map;
	int w;
	int h;
	GameMap();
	GameMap(GameState* state, int w, int h);
	// Create full GameMap -- which rooms, etc.
	void generate(GameState* state, int w, int h);
	void setSpawn(GameState* state);
	// Load data
	void loadJSON(std::string filename);
	// Map themed tileset loaded from json
	std::map<TileTerrain, std::vector<std::string>> spritelist;
	// Possible rooms loaded from json
	std::vector<MapData> possibleTiles;
	// Accessor in creating room connection structures
	std::vector<sf::Vector2i> getNeighboringTiles(sf::Vector2i loc);
	sf::Vector2i spawn;
};