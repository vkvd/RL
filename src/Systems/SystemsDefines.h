/*
  MACROS NOTE:
  ------------
  	These macros need to stay contained to 'Systems.cpp'
  	They're really bad and ugly, if they get used anywhere
  	else it will become spaghetti really fast

  	This rule violated on 1/23, wish me luck -- now in systemsdefines.h and used in multiple places it probably shouldnt
*/

// Get component list by shortened name
#define getComponents(a) (*(state->a.state))
// Shorthand for getComponents
#define gC(a) getComponents(a)
// getComponents size
#define gCSIZE(a) getComponents(a).size()
// getComponents size != 0
#define gCTEST(a) getComponents(a).size() > 0
// Opening iterator statement for clarity
#define COMPITER(a) if (a) 

/* With macros: General workflow is:
// Test for empty list:                               COMPITER(gCTEST(--entity list names--)) {
// iterate through entities with these components         for (auto ent : entities(gC(--entity list names--))) {
// Get reference to component of specific type                (type)& s = get<(type)>(ent);
// Then modify the component however needs to happen.    }
//                                                    }
*/