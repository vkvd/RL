#include "AISystems.h"

namespace AISystems {

	float v2fDist(sf::Vector2f a, sf::Vector2f b){ return std::sqrt(std::pow(a.x-b.x, 2) + std::pow(a.y-b.y, 2)); }

	// Delegator
	void enemyAI(AIData* data) {
		switch (data->enemy.ed.eclass){
			case EnemyClass::Fighter:
				runFighterAI(data);
			break;
			case EnemyClass::Ranged:
			break;
		}
	}

	// Enemy types
	void runFighterAI(AIData* data){
		switch (data->enemy.ed.behavior){
			case AIState::Idle:
				if (!data->enemy.timerExists("idle")){
					float time = (Engine::RNG->roll(5, 10) * 0.1f);
					data->enemy.addTimer("idle", time);
					Engine::Console()->debug("Adding idle timer for {} seconds", time);
				}
				if (data->enemy.timerExists("idle") && data->enemy.checkTimer("idle")){
					data->enemy.removeTimer("idle");
					data->enemy.ed.behavior = AIState::IdleWalk;
					Engine::Console()->debug("Dropping idle timer, starting to walk");
				}
				if (v2fDist(data->playerpos, sf::Vector2f(data->position.x, data->position.y)) < 300.0f){
					Engine::Console()->debug("Player noticed, approaching");
					data->enemy.ed.behavior = AIState::Approach;
				} else {
					data->sprite.playAnimation("idle", true);
					data->velocity.x = 0;
					data->velocity.y = 0;
				}
			break;
			case AIState::IdleWalk:
				if (v2fDist(data->playerpos,sf::Vector2f(data->position.x, data->position.y)) < 300.0f){
					if (data->enemy.timerExists("randomwalk")) {
						data->enemy.removeTimer("randomwalk");
					}
					Engine::Console()->debug("Player noticed, approaching");
					data->enemy.ed.behavior = AIState::Approach;
				} else {
					if (data->enemy.timerExists("randomwalk") && data->enemy.checkTimer("randomwalk")){
						data->enemy.removeTimer("randomwalk");
						data->enemy.ed.behavior = AIState::Idle;
						Engine::Console()->debug("Dropping timer, going back to idling");
					}
					if (!data->enemy.timerExists("randomwalk")){
						float time = (Engine::RNG->roll(5, 10) * 0.1f);
						data->enemy.addTimer("randomwalk", time);
						Engine::Console()->debug("Adding walking timer for {} seconds", time);
						int dirx = Engine::RNG->roll(-1, 1);
						int diry = Engine::RNG->roll(-1, 1);
						data->velocity.x = data->enemy.s.get().ms * dirx;
						data->velocity.y = data->enemy.s.get().ms * diry;
						data->sprite.playAnimation("move", true);
					}
				}
			break;
			case AIState::Approach: {
				TilePair tiles = updatePathfinding(data);
				
				sf::Vector2i playerTile, enemyTile;
				std::tie(playerTile, enemyTile) = tiles;
				
				runAtPlayer(data, enemyTile, playerTile);

				break;
			}
			case AIState::Chase:

			break;
			case AIState::Circle:

			break;
			case AIState::Retreat:

			break;
			case AIState::OnAttack:

			break;
			case AIState::OnTakeDamage:

			break;
		}
	}

	TilePair updatePathfinding(AIData* data){
		sf::Vector2i playerTile = (sf::Vector2i)data->playerpos;
		// TODO: by half the player size, hardcoded for now

		sf::Vector2i enemyTile  = sf::Vector2i(data->position.x, data->position.y);
		
		const int tw = data->state->gamemap.map[data->state->gamemapPos].data.tw;
		const int th = data->state->gamemap.map[data->state->gamemapPos].data.th;
		const int enemyW = data->sprite.width / 2;	
		const int enemyH = data->sprite.height / 2;

		playerTile.x = std::floor((playerTile.x) / tw);
		playerTile.y = std::floor((playerTile.y) / th);
		enemyTile.x  = std::floor((enemyTile.x) / tw);
		enemyTile.y  = std::floor((enemyTile.y) / th);
		
		data->enemy.pf.calcPath(enemyTile, playerTile);

		if (data->state->drawables.size() > 0) data->state->drawables.erase(data->state->drawables.begin() + data->state->drawables.size() -1);
		
		int length = data->enemy.pf.path.size();

		sf::VertexArray va(sf::LinesStrip, length + 1);


		va[0].position = sf::Vector2f(enemyTile.x * tw + tw/2, enemyTile.y * th + th/2);
		va[0].color = sf::Color::Red;
		for (int i = 1; i < length; ++i){
			PFNode* pfn = data->enemy.pf.path[i];
			va[i].position = sf::Vector2f(pfn->x*tw + (tw/2) ,pfn->y*th + (th/2));
			va[i].color = sf::Color::Red;
		}
		va[length].position = sf::Vector2f(playerTile.x * tw + tw/2, playerTile.y * th + th/2);
		va[length].color = sf::Color::Red;


		data->state->drawables.push_back(va);


		return std::tie(playerTile, enemyTile);
	}

	void runAtPlayer(AIData* data, sf::Vector2i enemyTile, sf::Vector2i playerTile){
		data->velocity.x = 0;
		data->velocity.y = 0;

		sf::Vector2i pathStart[2];
		pathStart[0] = sf::Vector2i ( data->enemy.pf.path[1]->x, data->enemy.pf.path[1]->y);
		pathStart[1] = sf::Vector2i ( data->enemy.pf.path[2]->x, data->enemy.pf.path[2]->y);

		GameMapTile* gmt = &data->state->gamemap.map[data->state->gamemapPos];
		std::map<sf::Vector2i, Tile>* tiles = &gmt->data.tiles;
		const int movespeed = data->enemy.s.get().ms;

		if (pathStart[1].x != enemyTile.x && pathStart[1].y != enemyTile.y) { // if the next two steps are diagonal
			if (pathStart[1].x > enemyTile.x && pathStart[1].y > enemyTile.y) { // Right and down 
				data->velocity.x =  movespeed; 
				data->velocity.y =  movespeed; 
			} 
			if (pathStart[1].x > enemyTile.x && pathStart[1].y < enemyTile.y) { // Right and up 
				data->velocity.x =  movespeed; 
				data->velocity.y = -movespeed; 
			} 
			if (pathStart[1].x < enemyTile.x && pathStart[1].y > enemyTile.y) { // Left and down 
				data->velocity.x = -movespeed; 
				data->velocity.y =  movespeed; 
			} 
			if (pathStart[1].x < enemyTile.x && pathStart[1].y < enemyTile.y) { // Left and up 
				data->velocity.x = -movespeed; 
				data->velocity.y = -movespeed; 
			}
		} else if (enemyTile.x != playerTile.x || enemyTile.y != playerTile.y) { // else, if the player and tile are in different spots
			if (pathStart[0].x > enemyTile.x) { // Enemy needs to move right
				data->velocity.x =  movespeed; 
				if ((*tiles)[enemyTile + sf::Vector2i(1,0)].data.terrain != TileTerrain::Wall){
					data->velocity.y = -movespeed;
				}
			}
			if (pathStart[0].x < enemyTile.x) { // ^ left
				data->velocity.x = -movespeed;
				if ((*tiles)[enemyTile + sf::Vector2i(-1,0)].data.terrain != TileTerrain::Wall){
					data->velocity.y = -movespeed;
				}
			} 
			if (pathStart[0].y > enemyTile.y) { // ^ down
				data->velocity.y =  movespeed;
				if ((*tiles)[enemyTile + sf::Vector2i(0,1)].data.terrain != TileTerrain::Wall){
					data->velocity.x = -movespeed;
				}
			} 
			if (pathStart[0].y < enemyTile.y) { // ^ up
				data->velocity.y = -movespeed;
				if ((*tiles)[enemyTile + sf::Vector2i(0,-1)].data.terrain != TileTerrain::Wall){
					data->velocity.x = -movespeed;
				}
			} 
		} 
		else if (enemyTile.x == playerTile.x || enemyTile.y == playerTile.y) { // much more precise up close - stop pathfinding and just run
			if (enemyTile.x == playerTile.x) {
				if (data->position.x < data->playerpos.x){
					data->velocity.x =  movespeed;
				}
				if (data->position.x > data->playerpos.x){
					data->velocity.x = -movespeed;
				}
			}
			if (enemyTile.y == playerTile.y) {
				if (data->position.y < data->playerpos.y){
					data->velocity.y =  movespeed;
				}
				if (data->position.y > data->playerpos.y){
					data->velocity.y = -movespeed;
				}
			}
		}
		//Engine::Console()->info("Pathfinding data:\n\tpt: {} {} \n\tet: {} {}\n\tepos: {} {}\n\tpos:  {} {}\n\tev: {} {}", playerTile.x, playerTile.y, enemyTile.x, enemyTile.y, data->position.x, data->position.y, data->playerpos.x, data->playerpos.y, data->velocity.x, data->velocity.y);
	}
}