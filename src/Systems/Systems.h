#pragma once
#include "ECS/ecs.h"
#include "Engine/Components.h"
#include "States/GameState.h"
#include "States/Game/DoorDirections.h"
using namespace ecs;

namespace Systems {
	// ----------------------------------------------------------
	// Systems
	//
	// The goal of this file is to provide major game functions, 
	// majorly concerning the entity system. 
	// ----------------------------------------------------------

	// Main calls
	// -----------------------------------------------------------------------------------------------
	// Called every frame. Do all non-drawing actions here.
	void update(GameState *state);
	// Called every frame. Do all drawing actions here.
	void draw(GameState *state);



	// Update subsystems
	// -----------------------------------------------------------------------------------------------
	// Update animation timers.
	void updateAnimations(GameState *state);
	// See if the player hit an exit if all of the enemies have been cleared out.
	void updateExits(GameState *state);

	// Controller subsystems
	// -----------------------------------------------------------------------------------------------
	// All player inputs and skills, etc. 
	void playerController(GameState *state);
	// All enemy inputs, skills, etc.
	void enemyController(GameState *state);
	// Deaths
	void handleDeaths(GameState *state);
	// Player death (in case of revive items, etc.)
	void handlePlayerDeath(GameState *state);
	// Stat management and buff linking
	void handleStats(GameState *state);
	// Inventories, 
	void handleInventories(GameState *state);

	// Draw subsystems
	// -----------------------------------------------------------------------------------------------
	// Draws each iterable entity
	void drawTextures(GameState *state);
	// What gets in the minimap view
	void drawMinimap(GameState *state);
	// What draws on the UI
	void drawGameHUD(GameState *state);
	// Fonts drawn outside the gameworld
	void drawUIText(GameState *state);
	// Draws current crosshair texture at mousepos
	void drawCrosshair(GameState *state);
	// Draws all primitive rectangle objects
	void drawRectangles(GameState *state);



	
	// Utilities:
	// -----------------------------------------------------------------------------------------------
	
	// -- General utilities:
	// ---- Calculates camera position for unlocked camera
	sf::Vector2f getCameraOffsetByMouse(GameState *state, sf::Vector2f playerpos);
	// ---- Handles window resizes and redraws
	void handleResize(GameState *state);
	// ---- Returns a number's sign
	template <typename T> int sgn(T val) { return (T(0) < val) - (val < T(0)); }
	
	// -- Drawing utilities:
	// ---- Never have to use setTexture every frame
	void singlePassSetSpriteTextures(GameState *state);
	// ---- Draw to framebuffer without camera
	CameraData releaseCamera(GameState *state);
	void lockCamera (GameState *state, CameraData data);

	// -- Collision utilities:
	// ---- Moves all colliders without testing anything.
	// ------ Possible fix for broken doors?
	void moveColliders(GameState *state);
	// ---- Moves all colliders and updates other components' positions
	void handleCollisions(GameState *state);
	// ---- Moves all colliders - subsystem of above
	// ------ cl1 and cl2 are duplicate lists that get mutated containing all collidable entities
	void resolveCollisions(std::vector<Collision*>* cl1, std::vector<Collision*>* cl2, float dt);

	// -- Room and Map utilities:
	// ---- Create room and player
	// ------ entryDir: Which direction the player came from.
	void makeRoom(GameState* state, EntryDir entryDir);

}