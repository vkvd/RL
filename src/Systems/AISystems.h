#pragma once
#include "Engine/Components.h"
#include "ECS/ecs.h"
#include "States/GameState.h"
#include "spdlog/spdlog.h"
#include "States/Game/EntityPrefabs.h"
#include "Engine/Random.h"

struct AIData {
	AIData(GameState* state, Position& position, Enemy& enemy, Velocity& velocity, Sprite& sprite, sf::Vector2f playerpos) :
		state(state),
		position(position),
		enemy(enemy),
		velocity(velocity),
		sprite(sprite),
		playerpos(playerpos) {}

	GameState*   state;
	Position&    position;
	Enemy&       enemy;
	Velocity&    velocity;
	Sprite&      sprite;
	sf::Vector2f playerpos;
};

namespace AISystems {

	typedef std::tuple<sf::Vector2i, sf::Vector2i> TilePair; 

	// Delegator
	// -----------------------
	void enemyAI(AIData* data);

	// Enemy types
	// -----------------------
	// -- Basic fighter AI
	void runFighterAI(AIData* data);

	// Behaviors and utilities
	// -----------------------
	// -- Returns the player and enemy tiles, as well as calculates the path.
	TilePair updatePathfinding(AIData* data);
	// -- Sets enemy velocity to rushdown the player.
	void runAtPlayer(AIData* data, sf::Vector2i enemyTile, sf::Vector2i playerTile);
}