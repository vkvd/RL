#include "Systems.h"
#include "spdlog/spdlog.h"
#include "States/Game/EntityPrefabs.h"
#include "Engine/Random.h"
#include "AISystems.h"

/*
  Systems.cpp:
  ------------
	Entity iteration systems go here.
	This serves as the basic game loop, as well as containing
	most of the game mechanics.
*/

#include "SystemsDefines.h" // All of the ugly macros are in here

namespace Systems {
	void update(GameState *state) {
		handleResize(state);
		if (state->isMapInitialized) {
			for (auto e: state->timers){
				e.update();
			}
			playerController  (state);
			enemyController   (state);
			moveColliders     (state);
			updateExits       (state);
			handleCollisions  (state);
			handleInventories (state);
			updateAnimations  (state);
			handleStats       (state);
			handlePlayerDeath (state);
			handleDeaths      (state);
		}
	}
	
	void draw(GameState *state) {
		drawTextures  (state);
		drawMinimap   (state);
		drawGameHUD   (state);
		// DEBUG: TODO: it's ugly
		for (auto& e : state->drawables) {
			Engine::Window->draw(e);
		}

		for (auto& e : state->draw_circles){
			Engine::Window->draw(e);
		}
		drawUIText    (state);
		drawCrosshair (state);
	}

	void updateAnimations(GameState *state) {
		COMPITER(gCTEST(sprites)) {
			for (auto ent : entities(gC(sprites))) {
				Sprite& s = get<Sprite>(ent);
				if (s.isAnimated)
					s.anim.update(sf::seconds(state->dt));
			}
		}
	}

	void updateExits(GameState * state)
	{
		COMPITER(gCTEST(collisions) && gCTEST(sprites)) {
			for (auto ent : entities(gC(collisions), gC(exits))) {
				Collision& exitCollider = get<Collision>(ent);
				Exit& e = get<Exit>(ent);
				// fixed by something that i dont understand
				if (e.Id < 0) {
					Engine::Console()->critical("door's broken again. i have actually no idea what causes this.\n if this comes back scrap the whole project");
					std::exit(1);
				}

				if (state->enemies.state->size() == 0) {
					e.active = true;
				}
				
				if (e.active) {
					// the if collider size checks are for initialization order issues where it checks before the room is made
					entity<Collision> p = *entities_find(state->playerID, gC(collisions));
					Collision& playerCollider = get<Collision>(p);
					sf::FloatRect r1;
					sf::FloatRect r2;
					r1 = playerCollider.rs[0];
					r2 = exitCollider.rs[0];
					// Detect if a collision occurred between two entities;
					bool collision = false;
					if (r1.left <= r2.left + r2.width &&
						r1.left + r1.width >= r2.left &&
						r1.top <= r2.top + r2.height &&
						r1.top + r1.height >= r2.top)
					{
						collision = true;
					}
					if (collision) {
						EntryDir entryDir;
						if (e.target == state->gamemapPos + sf::Vector2i(0, -1)) entryDir = EntryDir::Up;
						if (e.target == state->gamemapPos + sf::Vector2i(-1, 0)) entryDir = EntryDir::Left;
						if (e.target == state->gamemapPos + sf::Vector2i(1, 0)) entryDir = EntryDir::Right;
						if (e.target == state->gamemapPos + sf::Vector2i(0, 1)) entryDir = EntryDir::Down;
						state->model.tileStates[state->gamemapPos] = GMTState::Cleared;
						state->gamemapPos = e.target;
						state->model.firstlevel = false;
						Systems::makeRoom(state, entryDir);
					}
				}
			}
		}
	}

	// All player input that's not an engine command (like closing the game with escape)
	// is handled in this function. It's probably way too bloated, but it works well enough.
	void playerController(GameState *state) {
		
		COMPITER(gCTEST(positions)
			  && gCTEST(velocities)
			  && gCTEST(sprites)
			  && gCTEST(players)
			  && gCTEST(collisions)) {

			for (auto ent : entities(gC(positions), gC(velocities), gC(sprites), gC(players), gC(collisions)))
			{
				Player&   pl = get<Player>(ent);
				Position&  p = get<Position>(ent);
				Sprite&    s = get<Sprite>(ent);
				Collision& c = get<Collision>(ent);
				Velocity&  v = get<Velocity>(ent);
				if (!pl.s.isDead()) {
					if (pl.s.get().ms > pl.s.ms + pl.s.dex * 10) {
						Engine::Console()->info("Acted on by ms buff {}...", pl.s.get().ms - pl.s.ms - pl.s.dex * 10);
					}
					v.x = 0; v.y = 0;
					if (Engine::Input->isActive(Actions::LClick) || pl.i.equippedWeapon->lastSwingResult != SwingResult::Completed) { // buffering swings
						auto mposI = sf::Mouse::getPosition(*Engine::Window);
						auto mposF = sf::Vector2f(mposI.x, mposI.y);
						mposF = (sf::Vector2f)state->screenToWorld(mposI);
						const sf::Vector2f offsetPlayerModelSize = sf::Vector2f(p.x, p.y) + sf::Vector2f(32,32);
						
						float angle = std::atan2((mposF.y-offsetPlayerModelSize.y), (mposF.x-offsetPlayerModelSize.x));
						angle += (90/57.2958); // rotate by 90 degrees in radians
						

						pl.i.equippedWeapon->swing(state, angle, offsetPlayerModelSize, true);
					}
					if (Engine::Input->isActive(Actions::DBGF2)) {
						pl.s.addBuff("ms", 300, 1.0f);
						pl.s.addBuff("poison", 1, 3.01f);
					}
					if (Engine::Input->isActive(Actions::MMC)) {
						Prefabs::fighter(state, sf::Vector2f(100,100), 0);
					}
					if (Engine::Input->isActive(Actions::MMC)) {
						state->model.unlockedCamera = !state->model.unlockedCamera;
					}
					s.playAnimation("move", true);
					if (Engine::Input->isActive(Actions::Up)) {
						v.y += -pl.s.get().ms;
					}
					if (Engine::Input->isActive(Actions::Down)) {
						v.y += pl.s.get().ms;
					}
					if (Engine::Input->isActive(Actions::Left)) {
						v.x += -pl.s.get().ms;
					}
					if (Engine::Input->isActive(Actions::Right)) {
						v.x += pl.s.get().ms;
					}
					if (Engine::Input->isActive(Actions::NMH) && Engine::Input->isActive(Actions::NMV)) {
						s.playAnimation("idle", true);
					}
					sf::Vector2f ppos = sf::Vector2f(p.x, p.y);
					ppos += sf::Vector2f(s.height, s.width) / 2.0f;
					sf::Vector2f cpos = getCameraOffsetByMouse(state, ppos);
					state->cameraData.center = (sf::Vector2i)cpos;
					state->cameraData.updatePosition();
				}
			}
		}
	}

	// Enemy AI and such
	void enemyController(GameState *state){
		COMPITER(gCTEST(enemies) && gCTEST(players) && gCTEST(velocities)){
			sf::Vector2f playerpos;
			for (auto ent : entities(gC(players), gC(positions))){
				Position& p = get<Position>(ent);
				playerpos = sf::Vector2f(p.x, p.y);
			}
			for (auto ent : entities(gC(enemies), gC(positions), gC(velocities), gC(sprites))){
				Position& p = get<Position>(ent);
				Enemy& e = get<Enemy>(ent);
				Velocity& v = get<Velocity>(ent);
				Sprite& spr = get<Sprite>(ent);

				AIData data = AIData(state,p,e,v,spr,playerpos);
				AISystems::enemyAI(&data);
			}
		}
	}

	void handleDeaths(GameState * state)
	{
		COMPITER(gCTEST(enemies)){
			for (auto ent : entities(gC(enemies))){
				Enemy& en = get<Enemy>(ent);
				int id = en.Id;
				if (en.s.isDead()){
					// handle death
				}
			}
		}
	}

	void handlePlayerDeath(GameState * state)
	{
		COMPITER(gCTEST(players) && gCTEST(velocities) && gCTEST(sprites)){
			for (auto ent : entities(gC(players), gC(velocities), gC(sprites))){
				Player& p   = get<Player>(ent);
				Sprite& s   = get<Sprite>(ent);
				Velocity& v = get<Velocity>(ent);
				if (p.s.isDead()){
					state->model._tplayerStats = p.s;
					if (s.anim.isPlayingAnimation()) {
						s.anim.playAnimation("idle", false); // TODO: death animation, non looping
					}
					v.x = 0; v.y = 0;
				}
			}
		}
	}

	void handleStats(GameState * state)
	{
		COMPITER(gCTEST(players)) {
			for (auto ent : entities(gC(players))) {
				Player& pl = get<Player>(ent);
					if (!pl.s.isDead()){
					pl.s.updateBuffs(state->dt);
					bool poison = false; bool poisonTick = false;
					for (auto b : pl.s.getBuffsList()){
						if (b == "poison"){
							poison = true;
						}
						if (b == "poisonTick"){
							poisonTick = true;
						}
					}
					if (poison && !poisonTick){
						pl.s.applyDamage(pl.s.b.modifiers["poison"]);
						pl.s.addBuff("poisonTick", 0, 1.0f);
					}
				}
			}
		}

		COMPITER(gCTEST(enemies)) {
			for (auto ent : entities(gC(enemies))) {
				Enemy& e = get<Enemy>(ent);
				e.s.updateBuffs(state->dt);
			}
		}
	}

	void handleInventories(GameState *state){
		COMPITER(gCTEST(players)){
			for (auto ent : entities(gC(players))){
				Player& pl = get<Player>(ent); 
				pl.i.equippedWeapon->update();
			}
		}
		COMPITER(gCTEST(enemies)) {
			for (auto ent : entities(gC(enemies))){
				Enemy& e = get<Enemy>(ent);
				e.i.equippedWeapon->update();
			}
		}
	}

	void drawTextures(GameState *state) {
		COMPITER(gCTEST(positions) && gCTEST(sprites)) {
			for (auto ent : entities(gC(positions), gC(sprites))) {
				Position& p = get<Position>(ent);
				Sprite& s   = get<Sprite>  (ent);
				s.sprite.setPosition(p.x, p.y);
				if (p.x + s.width > state->cameraData.position.x &&
					p.x < state->cameraData.position.x + WINDOWSIZE.x &&
					p.y + s.height > state->cameraData.position.y &&
					p.y < state->cameraData.position.y + WINDOWSIZE.y) {
					if (s.isAnimated) {
						s.anim.animate(s.sprite);
						Engine::Window->draw(s.sprite);
					}
					else {
						Engine::Window->draw(s.sprite);
					}
				}
			}
		}
		COMPITER(gCTEST(positions) && gCTEST(sprites) && gCTEST(enemies)) {
			for (auto ent : entities(gC(positions), gC(sprites), gC(enemies))) {
				Position& p = get<Position>(ent);
				Sprite& s   = get<Sprite>  (ent);
				Enemy&  e   = get<Enemy>   (ent);
				s.sprite.setPosition(p.x, p.y);
				if (p.x + s.width > state->cameraData.position.x &&
					p.x < state->cameraData.position.x + WINDOWSIZE.x &&
					p.y + s.height > state->cameraData.position.y &&
					p.y < state->cameraData.position.y + WINDOWSIZE.y) {
					if (s.isAnimated) {
						s.anim.animate(s.sprite);
						Engine::Window->draw(s.sprite);
					}
					else {
						Engine::Window->draw(s.sprite);
					}
				}
			}
		}
	}

	// awful
	void drawMinimap(GameState *state) {

		CameraData oldCamera = releaseCamera(state);

		const sf::Color undiscCol = sf::Color(  0,  0,  0,  0);
		const sf::Color discCol   = sf::Color( 80, 80, 80,255);
		const sf::Color currCol   = sf::Color(255,255,255,255);
		const sf::Color clearCol  = sf::Color(180,180,180,255);
		const sf::Color itemCol   = sf::Color(  0,255,  0,255);
		const sf::Color bossCol   = sf::Color(255,  0,  0,255);

		const sf::Vector2i offsetTopRight = sf::Vector2i(20,20);
		const sf::Vector2i hudTotalSize   = sf::Vector2i(200,200);
		const sf::Vector2i minimapBorder  = sf::Vector2i(3,3);

		const float doorWidthPercent = 0.33;

		sf::Vector2f drawStartPos = (sf::Vector2f)WINDOWSIZE-(sf::Vector2f)(offsetTopRight+hudTotalSize);
		drawStartPos.y = WINDOWSIZE.y-drawStartPos.y-hudTotalSize.y;
		sf::RectangleShape minimapBackground;
		minimapBackground.setSize((sf::Vector2f)(minimapBorder*2) + (sf::Vector2f)hudTotalSize);
		minimapBackground.setPosition(drawStartPos - (sf::Vector2f)minimapBorder);
		minimapBackground.setFillColor(sf::Color::Black);

		Engine::Window->draw(minimapBackground);

		int roomSizeX = hudTotalSize.x / (state->gamemap.w);
		int roomSizeY = hudTotalSize.y / (state->gamemap.h);
		// make it square

		struct {int offset; bool resizedX;} resizeInfo;
		if (roomSizeX > roomSizeY){
			resizeInfo.offset = roomSizeX - roomSizeY;
			resizeInfo.resizedX = true;
			roomSizeX = roomSizeY;
		} else {
			resizeInfo.offset = roomSizeY - roomSizeX;
			resizeInfo.resizedX = false;
			roomSizeY = roomSizeX;
		}
		// TODO: minimap needs to be prettier

		roomSizeX -= 2;
		roomSizeY -= 2;

		sf::Vector2f roomSizeV(roomSizeX, roomSizeY);
		for (int y = 0; y < state->gamemap.h; y++){
			for (int x = 0; x < state->gamemap.w; x++){
				sf::RectangleShape mapTile;
				sf::RectangleShape mapTileBorder;
				// TODO: this is so hardcoded it's disgusting
				sf::Vector2f _dpos = sf::Vector2f((drawStartPos.x + x*roomSizeX + (2*x))+1, (drawStartPos.y + y*roomSizeY + (2*y))+1);

				mapTile.setSize(roomSizeV);
				mapTile.setPosition(_dpos);

				sf::RectangleShape upDoor    = sf::RectangleShape(sf::Vector2f(roomSizeX*doorWidthPercent, 2));
				sf::RectangleShape leftDoor  = sf::RectangleShape(sf::Vector2f(2, roomSizeY*doorWidthPercent));
				sf::RectangleShape rightDoor = sf::RectangleShape(sf::Vector2f(2, roomSizeY*doorWidthPercent));
				sf::RectangleShape downDoor  = sf::RectangleShape(sf::Vector2f(roomSizeX*doorWidthPercent, 2));
				upDoor.setPosition   (_dpos.x+roomSizeX*((1-doorWidthPercent)/2), _dpos.y-2);
				downDoor.setPosition (_dpos.x+roomSizeX*((1-doorWidthPercent)/2), _dpos.y+roomSizeY);
				rightDoor.setPosition(_dpos.x+roomSizeX, _dpos.y+roomSizeY*((1-doorWidthPercent)/2));
				leftDoor.setPosition (_dpos.x-2, _dpos.y+roomSizeY*((1-doorWidthPercent)/2));
				
				const sf::Color doorcolor = sf::Color(175, 110, 65);
				upDoor.setFillColor(doorcolor);
				downDoor.setFillColor(doorcolor);
				rightDoor.setFillColor(doorcolor);
				leftDoor.setFillColor(doorcolor);

				for (int i = 0; i < 4; i++){
					if (!state->gamemap.possibleTiles[state->gamemap.map[sf::Vector2i(x,y)].mapID].jumps[i]){
						switch(i){
							case 0: upDoor.setFillColor(sf::Color::Transparent); break;
							case 1: leftDoor.setFillColor(sf::Color::Transparent); break;
							case 2: rightDoor.setFillColor(sf::Color::Transparent); break;
							case 3: downDoor.setFillColor(sf::Color::Transparent); break;
						}
					}
				}

				switch ((int)state->model.tileStates[sf::Vector2i(x,y)]){
				case (int)GMTState::Undiscovered:
					mapTile.setFillColor(undiscCol);
					mapTileBorder.setOutlineColor(sf::Color::Transparent);
					upDoor.setFillColor(sf::Color::Transparent);
					downDoor.setFillColor(sf::Color::Transparent);
					rightDoor.setFillColor(sf::Color::Transparent);
					leftDoor.setFillColor(sf::Color::Transparent);
				break;
				case (int)GMTState::Discovered:
					mapTile.setFillColor(discCol);
					mapTileBorder.setOutlineColor(sf::Color::Transparent);
					upDoor.setFillColor(sf::Color::Transparent);
					downDoor.setFillColor(sf::Color::Transparent);
					rightDoor.setFillColor(sf::Color::Transparent);
					leftDoor.setFillColor(sf::Color::Transparent);
				break;
				case (int)GMTState::Current:
					mapTile.setFillColor(currCol);
				break;
				case (int)GMTState::Cleared:
					mapTile.setFillColor(clearCol);
				break;
				case (int)GMTState::Itemleft:
					mapTile.setFillColor(itemCol);
				break;
				case (int)GMTState::Boss:
					mapTile.setFillColor(bossCol);
				break;
				}
				Engine::Window->draw(mapTile);
				Engine::Window->draw(upDoor);
				Engine::Window->draw(downDoor);
				Engine::Window->draw(leftDoor);
				Engine::Window->draw(rightDoor);
			}
		}

		lockCamera(state, oldCamera);
	}

	// awful
	void drawGameHUD (GameState *state){
		sf::Color hpColor = sf::Color(230,55,55);
		sf::Color mpColor = sf::Color(55,135,175);
		sf::Color hpColorPoison = sf::Color(75,190,70);
		sf::Color missingHPColor = sf::Color(150,150,150);
		sf::Color missingMPColor = sf::Color(150,150,150);

		CameraData oldCamera = releaseCamera(state); 

		const sf::Vector2f offset    = sf::Vector2f(50,50);
		const sf::Vector2f totalsize = sf::Vector2f(300,75);
		const int margins = 4;
		sf::RectangleShape hudBackground = sf::RectangleShape(totalsize);
		hudBackground.setFillColor(sf::Color::Black);
		const sf::Vector2f bgDrawStart = sf::Vector2f(0,WINDOWSIZE.y-offset.y-totalsize.y) + sf::Vector2f(offset.x,0);
		hudBackground.setPosition(bgDrawStart);
		Engine::Window->draw(hudBackground);

		sf::RectangleShape healthBar;
		sf::RectangleShape manaBar;
		sf::RectangleShape missingHealthBar;
		sf::RectangleShape missingManaBar;

		const sf::Vector2f barsize = sf::Vector2f(totalsize.x-(margins*2), (totalsize.y/2)-((float)margins*1.5f));

		Stats stats; // Current stats with modifiers
		Stats pstatsnomods; // .get() doesn't preserve buffs -- use for grabbing buffs, cataloguing
		COMPITER(gCTEST(players)){
			for (auto ent : entities(gC(players))){
				Player& pl = get<Player>(ent);
				if (!pl.s.isDead()){
					stats = pl.s.get();
					pstatsnomods = pl.s;
				} else {
					stats = state->model._tplayerStats.get();
					pstatsnomods = state->model._tplayerStats;
				}
			}
		}
		if (gCTEST(players)){
			
		}
		sf::Vector2f HPFill = sf::Vector2f(barsize.x * ((float)stats.chp/(float)stats.mhp), barsize.y);
		sf::Vector2f MPFill = sf::Vector2f(barsize.x * ((float)stats.cmp/(float)stats.mmp), barsize.y);

		healthBar.setSize(HPFill);
		manaBar.setSize(MPFill);

		missingHealthBar.setSize(barsize);
		missingManaBar.setSize(barsize);

		healthBar.setFillColor(hpColor);
		manaBar.setFillColor(mpColor);
		missingHealthBar.setFillColor(missingHPColor);
		missingManaBar.setFillColor(missingMPColor);

		for (auto b : pstatsnomods.getBuffsList()){
			if (b == "poison"){
				healthBar.setFillColor(hpColorPoison);
			}
		}

		sf::Vector2f hpDrawPos = sf::Vector2f(bgDrawStart + sf::Vector2f(margins, margins));

		healthBar.setPosition(hpDrawPos);
		manaBar.setPosition(hpDrawPos + sf::Vector2f(0, barsize.y+margins));
		missingHealthBar.setPosition(hpDrawPos);
		missingManaBar.setPosition(hpDrawPos + sf::Vector2f(0, barsize.y+margins));

		std::string healthString = std::to_string(stats.chp)+"/"+std::to_string(stats.mhp);
		std::string manaString   = std::to_string(stats.cmp)+"/"+std::to_string(stats.mmp);
		UIText healthText        = UIText("main", 36, healthString);
		UIText manaText          = UIText("main", 36, manaString);
		healthText.centerAt((missingHealthBar.getSize()/2.0f)+missingHealthBar.getPosition());
		manaText  .centerAt((missingManaBar  .getSize()/2.0f)+missingManaBar  .getPosition());

		Engine::Window->draw(missingHealthBar);
		Engine::Window->draw(missingManaBar);
		Engine::Window->draw(healthBar);
		Engine::Window->draw(manaBar);

		Engine::Window->draw(healthText.text);
		Engine::Window->draw(manaText.text);

		lockCamera(state, oldCamera);
	}

	void drawUIText(GameState* state){
		for (UIText& t : state->ui_texts){
			Engine::Window->draw(t.text);
		}
	}

	void drawCrosshair(GameState *state) {
		auto mposI = sf::Mouse::getPosition(*Engine::Window);
		auto mposF = sf::Vector2f(mposI.x, mposI.y);
		sf::Vector2f screenPos = sf::Vector2f(state->screenToWorld(mposI));
		sf::Sprite s;
		sf::Texture t = ((*Engine::Textures)["cursor"]);
		int ox, oy;
		ox = t.getSize().x / 2;
		oy = t.getSize().y / 2;
		s.setTexture((*Engine::Textures)["cursor"]);
		s.setPosition(sf::Vector2f(screenPos) - sf::Vector2f(ox, oy));
		Engine::Window->draw(s);
	}

	void drawRectangle(GameState *state) {
		COMPITER(gCTEST(rectangles)){
			for (auto ent : entities(gC(rectangles))){
				Rectangle& r = get<Rectangle>(ent);
				Engine::Window->draw(r.drawable);
			}
		}
	}

	sf::Vector2f getCameraOffsetByMouse(GameState *state, sf::Vector2f playerpos) {
		if (state->model.unlockedCamera) {
			sf::Vector2f mpos = (sf::Vector2f)state->screenToWorld(sf::Mouse::getPosition(*Engine::Window));
			sf::Vector2f mpoint = sf::Vector2f(playerpos.x + mpos.x, playerpos.y + mpos.y) / 2.0f;
			sf::Vector2f offset = sf::Vector2f(playerpos.x + mpoint.x, playerpos.y + mpoint.y) / 2.0f;
			return offset;
		}
		return playerpos;
	}
	void handleResize(GameState *state) {
		if (Engine::Input->isActive(Actions::Resize) || Engine::Input->isActive(Actions::Fullscreen)) {
			// Fraction of the target rendering size based on 4/4
			//state->minimapBorderSize = sf::Vector2f((4.0f / 1280.0f)*WINDOWSIZE.x, (4.0f / 720.0f)*WINDOWSIZE.y);
			state->cameraData.window = (sf::Vector2f)WINDOWSIZE;
			state->cameraData.updatePosition();
		}
	}

	void singlePassSetSpriteTextures(GameState *state) {
		COMPITER(gCTEST(sprites)) {
			for (auto ent : entities(gC(sprites))) {
				Sprite& s = get<Sprite>(ent);
				s.sprite.setTexture(*s.texture);
			}
		}
	}

	CameraData releaseCamera(GameState *state){
		CameraData oldCamera = state->cameraData;
		state->cameraData.position = (sf::Vector2i(0,0));
		state->cameraData.updateCenter();
		state->cameraData.apply(state->camera);
		Engine::Window->setView(state->camera);
		return oldCamera;
	}

	void lockCamera(GameState *state, CameraData data){
		state->cameraData = data;
		state->cameraData.apply(state->camera);
		Engine::Window->setView(state->camera);
	}

	void moveColliders(GameState *state) {
		// Move colliders based on velocity
		COMPITER(gCTEST(collisions) && gCTEST(velocities)) {
			for (auto ent : entities(gC(collisions), gC(velocities), gC(positions))) {
				Collision& c = get<Collision>(ent);
				Velocity&  v = get<Velocity>(ent);
				Position&  p = get<Position>(ent);
				if (c.single) {
					c.offsetFromPosition[0] = sf::Vector2i(p.x - c.rs[0].left, p.y - c.rs[0].top);
					c.rs[0].left += v.x*state->dt;
					c.rs[0].top  += v.y*state->dt;
				}
			}
		}
	}
	void handleCollisions(GameState *state) {

		// Create collider lists
		std::vector<Collision*> cl1;
		//std::vector<Collision*> cl2;
		cl1.reserve(2000); //maybe more, just a guess number
		//cl2.reserve(2000);

		COMPITER(gCTEST(collisions)) {
			for (auto ent : entities(gC(collisions))) {
				Collision& c = get<Collision>(ent);
				if (c.single) cl1.push_back(&c); //else cl2.push_back(&c);
			}
			// Set lastvelocity accessible from only collision
			COMPITER(gCTEST(velocities)) {
				for (auto ent : entities(gC(collisions), gC(velocities))) {
					Velocity& v = get<Velocity>(ent);
					Collision& c = get<Collision>(ent);
					c.lvelocity = sf::Vector2f(v.x, v.y);
				}
			} // velocities

			// Move collider boxes
			resolveCollisions(&cl1, nullptr, state->dt); //nullptr -> cl2 when used

			// Move drawpositions
			COMPITER(gCTEST(positions)) {
				for (auto ent : entities(gC(collisions), gC(positions), gC(velocities))) {
					Position& p = get<Position>(ent);
					Collision& c = get<Collision>(ent);
					if (c.single) {
						p.x = c.rs[0].left - c.offsetFromPosition[0].x; 
						p.y = c.rs[0].top  - c.offsetFromPosition[0].y;
					}
				}
			} // positions
		} // collisions
	}

	void resolveEntities(std::vector<Collision*>* cl1, std::vector<Collision*>* cl2, int i, int j){
		// Detect if a collision occurred between two entities; statics don't matter so get initial case
		sf::FloatRect& r1 = (*cl1)[i]->rs[0];
		sf::FloatRect& r2 = (*cl1)[j]->rs[0];
		if (r1.left < r2.left + r2.width && 
			r1.left + r1.width > r2.left &&
			r1.top  < r2.top + r2.height && 
			r1.top  + r1.height > r2.top)
		{ // If there is a collision:

			// static: and they should not move
			// Should they ever have to, add details here:
			//if ((*cl1)[i]->_static == true && (*cl1)[j]->_static == true) { }

			// static: xor
			if ((((*cl1)[j]->_static == true && (*cl1)[i]->_static == false)) ||
				(((*cl1)[j]->_static == false && (*cl1)[i]->_static == true)) )
			{
				// Depending on static, flip objects for solver at later
				if ((*cl1)[j]->_static == true && (*cl1)[i]->_static == false) {
					sf::FloatRect& r2 = (*cl1)[i]->rs[0];
					sf::FloatRect& r1 = (*cl1)[j]->rs[0];
				}

				// Tests for directions
				float t1 = r1.left + r1.width  - r2.left;
				float t2 = r1.top  + r1.height - r2.top;
				float t3 = r2.left + r2.width  - r1.left;
				float t4 = r2.top  + r2.height - r1.top;

				// Assuming r1 is static and r2 is dynamic
				int slvx = sgn((*cl1)[j]->lvelocity.x);
				int slvy = sgn((*cl1)[j]->lvelocity.y);
				if (slvx ==  1 && slvy ==  1) { // -> V
					if      (t1 > t2) r2.left = r1.left - r2.width; 
					else if (t2 > t1) r2.top  = r1.top  - r2.height; 
					// else {
					// 	r2.left = r1.left - r2.width;
					// 	r2.top  = r1.top  - r2.height; 
					// }
				}
				if (slvx ==  1 && slvy == -1) { // -> ^
					if      (t1 > t4) r2.left = r1.left - r2.width; 
					else if (t4 > t1) r2.top  = r1.top  + r2.height; 
					// else {
					// 	r2.left = r1.left - r2.width;
					// 	r2.top  = r1.top  + r2.height; 
					// }
				}
				if (slvx == -1 && slvy ==  1) { // <- V
					if      (t3 > t2) r2.left = r1.left + r2.width; 
					else if (t2 > t3) r2.top  = r1.top  - r2.height; 
					// else {
					// 	r2.left = r1.left + r2.width;
					// 	r2.top  = r1.top  - r2.height; 
					// }
				}
				if (slvx == -1 && slvy == -1) { // <- ^
					if      (t3 > t4) r2.left = r1.left + r2.width; 
					else if (t3 < t4) r2.top  = r1.top  + r2.height; 
					// else {
					// 	r2.left = r1.left + r2.width; 
					// 	r2.top  = r1.top  + r2.height; 
					// }
				}
				if (slvx ==  1 && slvy == 0)
					r2.left = r1.left - r2.width; 
				if (slvx == -1 && slvy == 0)
					r2.left = r1.left + r2.width;
				if (slvx ==  0 && slvy == 1)
					r2.top  = r1.top  - r2.height; 
				if (slvx ==  0 && slvy == -1)
					r2.top  = r1.top  + r2.height; 
			}
			
			// Static not
			if ((*cl1)[j]->_static == false && (*cl1)[i]->_static == false) {

			}
		}
	}

	//cl1: simple (single rect objects)
	//cl2: complex, multirect objects
	// there is no cl2 support yet,maybe ever
	void resolveCollisions(std::vector<Collision*>* cl1, std::vector<Collision*>* cl2, float dt) {
		for (unsigned i = 0; i < (*cl1).size(); i++) {
			for (unsigned j = i + 1; j < (*cl1).size(); j++) {
				if (i != j) {
					resolveEntities(cl1, cl2, i, j);
				}//if i!=j
			}// for j
		}// for i
	}
	void makeRoom(GameState * state, EntryDir entryDir)
	{
		// TODO: DEBUG: this isnt needed outside of testing
		state->drawables.clear();
		// Freeze drawing to prevent screentearing
		Engine::drawFreeze = true;
		Engine::Console()->debug("Loading precooked tilemap at position (x:{}, y:{})", state->gamemapPos.x, state->gamemapPos.y);
		Stats s;
		Buffs b;
		Inventory i;
		// if the gamemodel (subj. to change for now) is a game in progress
		// copy over stats to new entity
		if (!state->model.firstlevel) {
			COMPITER(gCTEST(players)) {
				entity<Player> p = *entities_find(state->playerID, gC(players));
				s = get<Player>(p).s;
				b = s.b;
				i = get<Player>(p).i;
			}
		}
		else {
			// halfheartedly set stats in a bad way, TODO: fix this lmao
			s.ms = 400;
			b.modifiers.clear();
			b.timers.clear();
		}
		
		// if a past player entity has been created,
		// delete the current before creating a new
		if (state->playerID != 0) {
			COMPITER(gCTEST(players)) {
				for (auto ent : entities(gC(players))) {
					Player& p = get<Player>(ent);
					state->deleteEntity(p.Id);
				}
			}
		}

		// Reset components
		state->updateComponentPointers();
		
		sf::Vector2f playerSpawnPos;
		int mapw = state->gamemap.map[state->gamemapPos].data.w;
		int maph = state->gamemap.map[state->gamemapPos].data.h;
		int th = state->gamemap.map[state->gamemapPos].data.th;
		int tw = state->gamemap.map[state->gamemapPos].data.tw;
		//Entrydir
		//  0
		//1   2
		//  3
		// Spawn
		//  3
		//2   1
		//  0


		// Note: keep the original sizes for these even due to a 
		//     reversing trick. its a bad idea but it works
		// We'll have up to 4 normal spawns: each possible door
		std::vector<sf::Vector2f> pspawns; pspawns.reserve(4);
		// 5 spawns - all possibilities and the last option, drop in center
		std::vector<sf::Vector2f> spawns;  spawns.resize  (4);
		// Create each possible spawn from the doors' locations (so that it feels like you walk out of them)
		for (auto ent : entities(gC(exits), gC(positions))) {
			Position& e = get<Position>(ent);
			pspawns.push_back(sf::Vector2f(e.x, e.y));
		}
		// If the jump exists (if the room has that door) copy it over as a definite spawn
		for (int j = 0, i = 0; i < 4; i++) {
			if (state->gamemap.map[state->gamemapPos].jumps[i]) {
				spawns[i] = pspawns[j];
				j++;
			}
			// Otherwise leave it blank
			else {
				spawns[i] = sf::Vector2f(0,0);
			}
		}
		// Flip the list because entrydir and spawns are reversed
		// Possible bug: if spawns is an odd number this is broken... (bad)
		// however, spawns in resized to 4 so it works... for now     (good)
		std::reverse(spawns.begin(), spawns.end());
		int idx=0; for (auto& s : spawns) {
			Engine::Console()->debug("all possible spawns::x{} y{} :: index {}", s.x, s.y, idx);
			idx++;
		}
		MapData d = state->gamemap.map[state->gamemapPos].data;
		
		// 5th element of spawns is drop in center
		spawns.push_back(sf::Vector2f(d.w*d.tw/2, d.h*d.th/2));
		playerSpawnPos = spawns[(int)entryDir];
		Engine::Console()->debug("you're in this one : {}",(int)entryDir);
		// Entryoffset is how far it places you from the door so you dont get stuck in door warps
		// as of 2/21: now fixed for 0, doors are smaller colliding...
		const int entryoffset = 0;
		if (entryDir == EntryDir::Down) {
			playerSpawnPos += sf::Vector2f(0,  th+entryoffset);
		}
		if (entryDir == EntryDir::Left) {
			playerSpawnPos += sf::Vector2f(-tw-entryoffset, 0);
		}
		if (entryDir == EntryDir::Up) {
			playerSpawnPos += sf::Vector2f(0, -th-entryoffset);
		}
		if (entryDir == EntryDir::Right) {
			playerSpawnPos += sf::Vector2f(tw+entryoffset,  0);
		}
		Engine::Console()->debug("SPAWN ENTRYDIR :: {} {} {}", (int)entryDir, playerSpawnPos.x, playerSpawnPos.y);
		Engine::Console()->debug("Entered room (x:{}, y:{}) from direction ({}), room id ({})", state->gamemapPos.x, state->gamemapPos.y, (int)entryDir, state->gamemap.map[state->gamemapPos].mapID);
		Engine::Console()->debug("Spawned in room at position x: {} y: {}", playerSpawnPos.x, playerSpawnPos.y);
		state->playerID = Prefabs::player(state, playerSpawnPos, s, i);
		state->model.tileStates[state->gamemapPos]=GMTState::Current;
		for (int i = 0; i < 4; i++){
			if (state->gamemap.possibleTiles[state->gamemap.map[state->gamemapPos].mapID].jumps[i]){
				sf::Vector2i offset; switch (i){
				case 0: offset=sf::Vector2i( 0,-1); break;
				case 1: offset=sf::Vector2i(-1, 0); break;
				case 2: offset=sf::Vector2i( 1, 0); break;
				case 3: offset=sf::Vector2i( 0, 1); break;
				}
				if (state->model.tileStates[state->gamemapPos + offset] == GMTState::Undiscovered){
					state->model.tileStates[state->gamemapPos + offset] =  GMTState::Discovered;
				}
			}
		}
		Engine::drawFreeze = false;
	}
}