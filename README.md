# RL   
A work in progress realtime roguelite.   


### src/   
All the source files and headers lumped together in folders.   

### include/  
All the third-party libraries included.   

### build/  
All the premake/makefiles involved as well as clang's databases.    

### Dependencies
`clang`      
Arch Linux AUR Packages:     
	- `sfml-git`    
	- `thor-git`     
Windows:     
	- Use visual studio to link in SFML and Thor.    
	- Use the include dirs to point at the folder.    

### Autocomplete:    
Steps:    
	- Use `bear` to generate a compilation database    
	- Use `EasyClangComplete` in sublime (note: had to set `libclang: false`) and point it to the database    
	- That should be it   